package com.weifly.weistock.bo.record;

/**
 * 交易记录配对
 *
 * @author weifly
 * @since 2020/01/15
 */
public class RecordPairBO {

    private int index; // 配对序号
    private int number; // 配对数量

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
