package com.weifly.weistock.bo;

/**
 * 差值、比率
 *
 * @author weifly
 * @since 2021/07/01
 */
public class DiffRateBO {

    private double diff;
    private double rate;

    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
