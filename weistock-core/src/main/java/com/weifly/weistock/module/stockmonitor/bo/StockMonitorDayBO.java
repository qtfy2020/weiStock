package com.weifly.weistock.module.stockmonitor.bo;

/**
 * 股票监控日数据
 *
 * @author weifly
 * @since 2021/6/9
 */
public class StockMonitorDayBO {

    /**
     * 日期 20200731
     */
    private String day;

    /**
     * 收盘价
     */
    private double close;

    /**
     * 差额
     */
    private double diff;

    /**
     * 涨幅
     */
    private double rate;

    /**
     * 上涨指标：前30日最低价所在日期
     */
    private String up_lowest_day;

    /**
     * 上涨指标：前30日最低价
     */
    private double up_lowest_price;

    /**
     * 上涨指标：差额（当天收盘价 - 前30日最低价）
     */
    private double up_diff;

    /**
     * 上涨指标：涨幅（差额 / 前30日最低价）
     */
    private double up_rate;

    /**
     * 上涨指标：涨幅all百分比
     */
    private double up_rate_all_percent;

    /**
     * 下跌指标：前30日最高价所在日期
     */
    private String down_highest_day;

    /**
     * 下跌指标：前30日最高价
     */
    private double down_highest_price;

    /**
     * 下跌指标：差额（当天收盘价 - 前30日最高价）
     */
    private double down_diff;

    /**
     * 下跌指标：跌幅（差额 / 前30日最高价）
     */
    private double down_rate;

    /**
     * 下跌指标：跌幅all百分比
     */
    private double down_rate_all_percent;

    /**
     * 上涨百分比 - 下跌百分比
     */
    private double diff_rate_percent;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getUp_lowest_day() {
        return up_lowest_day;
    }

    public void setUp_lowest_day(String up_lowest_day) {
        this.up_lowest_day = up_lowest_day;
    }

    public double getUp_lowest_price() {
        return up_lowest_price;
    }

    public void setUp_lowest_price(double up_lowest_price) {
        this.up_lowest_price = up_lowest_price;
    }

    public double getUp_diff() {
        return up_diff;
    }

    public void setUp_diff(double up_diff) {
        this.up_diff = up_diff;
    }

    public double getUp_rate() {
        return up_rate;
    }

    public void setUp_rate(double up_rate) {
        this.up_rate = up_rate;
    }

    public double getUp_rate_all_percent() {
        return up_rate_all_percent;
    }

    public void setUp_rate_all_percent(double up_rate_all_percent) {
        this.up_rate_all_percent = up_rate_all_percent;
    }

    public String getDown_highest_day() {
        return down_highest_day;
    }

    public void setDown_highest_day(String down_highest_day) {
        this.down_highest_day = down_highest_day;
    }

    public double getDown_highest_price() {
        return down_highest_price;
    }

    public void setDown_highest_price(double down_highest_price) {
        this.down_highest_price = down_highest_price;
    }

    public double getDown_diff() {
        return down_diff;
    }

    public void setDown_diff(double down_diff) {
        this.down_diff = down_diff;
    }

    public double getDown_rate() {
        return down_rate;
    }

    public void setDown_rate(double down_rate) {
        this.down_rate = down_rate;
    }

    public double getDown_rate_all_percent() {
        return down_rate_all_percent;
    }

    public void setDown_rate_all_percent(double down_rate_all_percent) {
        this.down_rate_all_percent = down_rate_all_percent;
    }

    public double getDiff_rate_percent() {
        return diff_rate_percent;
    }

    public void setDiff_rate_percent(double diff_rate_percent) {
        this.diff_rate_percent = diff_rate_percent;
    }
}
