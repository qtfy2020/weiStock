package com.weifly.weistock.module.stockmonitor.bo;

import com.weifly.weistock.core.constant.StockCatalogEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * 股票监控配置
 *
 * @author weifly
 * @since 2021/6/9
 */
public class StockMonitorConfigBO {

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 股票分类
     */
    private StockCatalogEnum stockCatalog;

    /**
     * 交易股票代码
     */
    private String tradeStockCode;

    /**
     * 记录列表
     */
    private List<StockMonitorDayBO> dayList = new ArrayList<>();

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public StockCatalogEnum getStockCatalog() {
        return stockCatalog;
    }

    public void setStockCatalog(StockCatalogEnum stockCatalog) {
        this.stockCatalog = stockCatalog;
    }

    public String getTradeStockCode() {
        return tradeStockCode;
    }

    public void setTradeStockCode(String tradeStockCode) {
        this.tradeStockCode = tradeStockCode;
    }

    public List<StockMonitorDayBO> getDayList() {
        return dayList;
    }

    public void setDayList(List<StockMonitorDayBO> dayList) {
        this.dayList = dayList;
    }
}
