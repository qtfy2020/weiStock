package com.weifly.weistock.module.stockdata.bo;

/**
 * 获取天数据请求
 *
 * @author weifly
 * @since 2021/06/16
 */
public class GetDayListRequest {

    private String stockCode;
    private String date; // 发生日期
    private int limit; // 返回记录数

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
