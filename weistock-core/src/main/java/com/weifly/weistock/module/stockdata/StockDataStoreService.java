package com.weifly.weistock.module.stockdata;

import com.weifly.weistock.core.config.ConfigPathSetter;
import com.weifly.weistock.core.market.bo.StockKLineBO;

import java.util.List;

/**
 * 数据存储服务
 *
 * @author weifly
 * @since 2021/6/9
 */
public interface StockDataStoreService extends ConfigPathSetter {

    /**
     * 加载股票数据列表
     */
    List<StockKLineBO> loadStockDataList();

    /**
     * 加载股票数据
     */
    StockKLineBO loadStockData(String stockCode);

    /**
     * 保存股票数据
     */
    void saveStockData(StockKLineBO stockKLineBO);
}
