package com.weifly.weistock.module.stockmonitor;

import com.weifly.weistock.core.config.ConfigPathSetter;
import com.weifly.weistock.module.stockmonitor.bo.StockMonitorConfigBO;

import java.util.List;

/**
 * 股票监控存储服务
 *
 * @author weifly
 * @since 2021/6/21
 */
public interface StockMonitorStoreService extends ConfigPathSetter {

    /**
     * 加载配置列表
     */
    List<StockMonitorConfigBO> loadStockMonitorConfigList();

    /**
     * 保存配置
     */
    void saveStockMonitorConfig(StockMonitorConfigBO stockMonitorConfig);
}
