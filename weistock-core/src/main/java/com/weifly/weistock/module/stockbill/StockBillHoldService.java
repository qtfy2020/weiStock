package com.weifly.weistock.module.stockbill;

import com.weifly.weistock.bo.MergeRecordResult;
import com.weifly.weistock.module.stockbill.bo.GetStockRecordRequest;
import com.weifly.weistock.module.stockbill.bo.StockHoldSummaryBO;
import com.weifly.weistock.module.stockbill.bo.StockRecordBO;
import com.weifly.weistock.module.stockbill.bo.StockSummaryBO;

import java.util.List;

/**
 * 股票交易记录管理服务
 *
 * @author weifly
 * @since 2020/01/15
 */
public interface StockBillHoldService {

    /**
     * 合并交易记录
     */
    MergeRecordResult mergeRecordList(List<StockRecordBO> recordList);

    /**
     * 获得股票列表
     */
    List<StockSummaryBO> getStockList();

    /**
     * 获得股票信息
     */
    StockSummaryBO getStockSummary(String stockCode);

    /**
     * 获得股票净持仓统计信息
     */
    StockHoldSummaryBO getStockHold(String stockCode);

    /**
     * 更新股票列表
     */
    void updateStockList(List<StockSummaryBO> stockList);

    /**
     * 加载记录列表
     */
    List<StockRecordBO> getRecordList(GetStockRecordRequest queryRequest);
}
