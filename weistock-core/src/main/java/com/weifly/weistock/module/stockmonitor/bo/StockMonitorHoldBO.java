package com.weifly.weistock.module.stockmonitor.bo;

/**
 * 股票监控hold
 *
 * @author weifly
 * @since 2021/7/12
 */
public class StockMonitorHoldBO {

    private StockMonitorConfigBO stockMonitorConfig; // 配置
    private StockMonitorStatusBO stockMonitorStatus; // 状态

    public StockMonitorHoldBO(StockMonitorConfigBO stockMonitorConfig) {
        this.stockMonitorConfig = stockMonitorConfig;
    }

    public StockMonitorConfigBO getStockMonitorConfig() {
        return stockMonitorConfig;
    }

    public void setStockMonitorConfig(StockMonitorConfigBO stockMonitorConfig) {
        this.stockMonitorConfig = stockMonitorConfig;
    }

    public synchronized StockMonitorStatusBO getStockMonitorStatus() {
        return stockMonitorStatus;
    }

    public synchronized void setStockMonitorStatus(StockMonitorStatusBO stockMonitorStatus) {
        this.stockMonitorStatus = stockMonitorStatus;
    }
}
