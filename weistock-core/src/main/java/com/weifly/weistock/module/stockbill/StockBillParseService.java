package com.weifly.weistock.module.stockbill;

import com.weifly.weistock.module.stockbill.bo.StockRecordBO;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 解析股票对账单
 *
 * @author weifly
 * @since 2020/01/15
 */
public interface StockBillParseService {

    /**
     * 解析交易记录
     */
    List<StockRecordBO> parseRecord(File recordFile) throws IOException;

    /**
     * 解析交易记录
     */
    List<StockRecordBO> parseRecord(InputStream input) throws IOException;
}