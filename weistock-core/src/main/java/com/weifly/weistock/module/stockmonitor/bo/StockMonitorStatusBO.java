package com.weifly.weistock.module.stockmonitor.bo;

/**
 * 股票监控状态数据
 *
 * @author weifly
 * @since 2021/7/12
 */
public class StockMonitorStatusBO {

    private String priceDate;               // 价格产生日期
    private String priceTime;               // 价格产生时间
    private Double nowPrice;                // 当前价格
    private Double nowRate;                 // 涨跌幅
    private double up_rate;                 // 上涨指标：涨幅（差额 / 前30日最低价）
    private double up_rate_all_percent;     // 上涨指标：涨幅all百分比
    private double down_rate;               // 下跌指标：跌幅（差额 / 前30日最高价）
    private double down_rate_all_percent;   // 下跌指标：跌幅all百分比
    private double diff_rate_percent;       // 百分比差额（上涨百分比 - 下跌百分比）
    private String tradeStockCode;          // 交易标的，代码
    private String tradeStockName;          // 交易标的，名称
    private Double tradeNowPrice;           // 交易标的，当前价格
    private Double tradeNowRate;            // 交易标的，涨跌幅
    private Double tradeLastPrice;          // 交易标的，最后成交价
    private Double tradeLastRate;           // 交易标的，最后涨跌幅

    public String getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate;
    }

    public String getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(String priceTime) {
        this.priceTime = priceTime;
    }

    public Double getNowPrice() {
        return nowPrice;
    }

    public void setNowPrice(Double nowPrice) {
        this.nowPrice = nowPrice;
    }

    public Double getNowRate() {
        return nowRate;
    }

    public void setNowRate(Double nowRate) {
        this.nowRate = nowRate;
    }

    public double getUp_rate() {
        return up_rate;
    }

    public void setUp_rate(double up_rate) {
        this.up_rate = up_rate;
    }

    public double getUp_rate_all_percent() {
        return up_rate_all_percent;
    }

    public void setUp_rate_all_percent(double up_rate_all_percent) {
        this.up_rate_all_percent = up_rate_all_percent;
    }

    public double getDown_rate() {
        return down_rate;
    }

    public void setDown_rate(double down_rate) {
        this.down_rate = down_rate;
    }

    public double getDown_rate_all_percent() {
        return down_rate_all_percent;
    }

    public void setDown_rate_all_percent(double down_rate_all_percent) {
        this.down_rate_all_percent = down_rate_all_percent;
    }

    public double getDiff_rate_percent() {
        return diff_rate_percent;
    }

    public void setDiff_rate_percent(double diff_rate_percent) {
        this.diff_rate_percent = diff_rate_percent;
    }

    public String getTradeStockCode() {
        return tradeStockCode;
    }

    public void setTradeStockCode(String tradeStockCode) {
        this.tradeStockCode = tradeStockCode;
    }

    public String getTradeStockName() {
        return tradeStockName;
    }

    public void setTradeStockName(String tradeStockName) {
        this.tradeStockName = tradeStockName;
    }

    public Double getTradeNowPrice() {
        return tradeNowPrice;
    }

    public void setTradeNowPrice(Double tradeNowPrice) {
        this.tradeNowPrice = tradeNowPrice;
    }

    public Double getTradeNowRate() {
        return tradeNowRate;
    }

    public void setTradeNowRate(Double tradeNowRate) {
        this.tradeNowRate = tradeNowRate;
    }

    public Double getTradeLastPrice() {
        return tradeLastPrice;
    }

    public void setTradeLastPrice(Double tradeLastPrice) {
        this.tradeLastPrice = tradeLastPrice;
    }

    public Double getTradeLastRate() {
        return tradeLastRate;
    }

    public void setTradeLastRate(Double tradeLastRate) {
        this.tradeLastRate = tradeLastRate;
    }
}
