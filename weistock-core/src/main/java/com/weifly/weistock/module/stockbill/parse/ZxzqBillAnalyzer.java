package com.weifly.weistock.module.stockbill.parse;

import com.weifly.weistock.core.common.StockException;
import com.weifly.weistock.core.constant.StockBillColumnTypeEnum;
import com.weifly.weistock.module.stockbill.bo.BillHeadColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 中信证券 对账单分析器
 *
 * @author weifly
 * @since 2021/05/14
 */
public class ZxzqBillAnalyzer extends AbstractBillAnalyzer {

    private Logger log = LoggerFactory.getLogger(ZxzqBillAnalyzer.class);

    @Override
    public List<BillHeadColumn> analyzeHead(List<String> lineList) {
        String headLine = null;
        for (String line : lineList) {
            if (line.startsWith("发生日期")) {
                headLine = line;
                break;
            }
        }
        if (headLine == null) {
            return null;
        }
        String[] columnNames = headLine.split("\\s+");
        if (!columnNames[0].equals("发生日期") || !columnNames[1].equals("成交时间") || !columnNames[2].equals("业务名称")) {
            return null; // 不满足条件
        }
        List<BillHeadColumn> columnList = new ArrayList<>();
        for (int i = 0; i < columnNames.length; i++) {
            this.makeOneColumn(columnList, columnNames[i], i);
        }
        return columnList;
    }

    private void makeOneColumn(List<BillHeadColumn> columnList, String columnName, int columnIndex) {
        StockBillColumnTypeEnum columnType = null;
        if (columnName.equals("发生日期")) {
            columnType = StockBillColumnTypeEnum.DATE;
        } else if (columnName.equals("成交时间")) {
            columnType = StockBillColumnTypeEnum.TIME;
        } else if (columnName.equals("业务名称")) {
            columnType = StockBillColumnTypeEnum.BUSINESS_NAME;
        } else if (columnName.equals("证券代码")) {
            columnType = StockBillColumnTypeEnum.STOCK_CODE;
        } else if (columnName.equals("证券名称")) {
            columnType = StockBillColumnTypeEnum.STOCK_NAME;
        } else if (columnName.equals("成交价格")) {
            columnType = StockBillColumnTypeEnum.TRADE_PRICE;
        } else if (columnName.equals("成交数量")) {
            columnType = StockBillColumnTypeEnum.TRADE_NUMBER;
        } else if (columnName.equals("成交金额")) {
            columnType = StockBillColumnTypeEnum.TRADE_AMOUNT;
        } else if (columnName.equals("股份余额")) {
            columnType = StockBillColumnTypeEnum.AFTER_NUMBER;
        } else if (columnName.equals("手续费")) {
            columnType = StockBillColumnTypeEnum.FEE_SERVICE;
        } else if (columnName.equals("印花税")) {
            columnType = StockBillColumnTypeEnum.FEE_STAMP;
        } else if (columnName.equals("过户费")) {
            columnType = StockBillColumnTypeEnum.FEE_TRANSFER;
        } else if (columnName.equals("附加费")) {
            columnType = StockBillColumnTypeEnum.FEE_EXTRA;
        } else if (columnName.equals("交易所清算费")) {
            columnType = StockBillColumnTypeEnum.FEE_CLEAR;
        } else if (columnName.equals("发生金额")) {
            columnType = StockBillColumnTypeEnum.CLEAR_AMOUNT;
        } else if (columnName.equals("资金本次余额")) {
            columnType = StockBillColumnTypeEnum.AFTER_AMOUNT;
        } else if (columnName.equals("委托编号")) {
            columnType = StockBillColumnTypeEnum.ENTRUST_CODE;
        } else if (columnName.equals("流水号")) {
            columnType = StockBillColumnTypeEnum.TRANSACTION_NUMBER;
        } else if (columnName.equals("股东代码")) {
            columnType = StockBillColumnTypeEnum.STOCK_HOLDER_CODE;
        } else if (columnName.equals("资金帐号")) {
            columnType = StockBillColumnTypeEnum.FUND_ACCOUNT;
        } else if (columnName.equals("币种")) {
            columnType = StockBillColumnTypeEnum.CURRENCY;
        } else if (columnName.equals("备注")) {
            columnType = StockBillColumnTypeEnum.REMARK;
        }
        if (columnType == null) {
            throw new StockException("不支持的列名：" + columnName);
        }
        BillHeadColumn headColumn = new BillHeadColumn();
        headColumn.setColumnType(columnType);
        headColumn.setColumnName(columnName);
        headColumn.setColumnIndex(columnIndex);
        columnList.add(headColumn);
    }
}
