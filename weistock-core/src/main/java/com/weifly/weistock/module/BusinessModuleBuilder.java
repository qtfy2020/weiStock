package com.weifly.weistock.module;

import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.core.util.ModuleBuilder;
import com.weifly.weistock.module.stockbill.StockBillHoldService;
import com.weifly.weistock.module.stockbill.StockBillStoreService;
import com.weifly.weistock.module.stockbill.bo.StockSummaryBO;
import com.weifly.weistock.module.stockdata.StockDataHoldService;
import com.weifly.weistock.module.stockdata.StockDataStoreService;
import com.weifly.weistock.module.stockmonitor.StockMonitorHoldService;
import com.weifly.weistock.module.stockmonitor.StockMonitorStoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

/**
 * 业务模块builder
 *
 * @author weifly
 * @since 2021/6/11
 */
public class BusinessModuleBuilder implements ModuleBuilder {

    private Logger log = LoggerFactory.getLogger(BusinessModuleBuilder.class);

    @Override
    public void build(String configPath) {
        String dataPath = new File(configPath, "data").getAbsolutePath();
        log.info("构建历史数据模块: " + dataPath);
        this.stockDataStoreService.setConfigPath(dataPath);
        List<StockKLineBO> stockDataList = this.stockDataStoreService.loadStockDataList();
        this.stockDataHoldService.updateStockDataList(stockDataList);

        String stockMonitorPath = new File(configPath, "stockMonitor").getAbsolutePath();
        log.info("构建股票监控模块：" + stockMonitorPath);
        this.stockMonitorStoreService.setConfigPath(stockMonitorPath);
        this.stockMonitorHoldService.updateStockMonitorList(this.stockMonitorStoreService.loadStockMonitorConfigList());

        String billPath = new File(configPath, "bill").getAbsolutePath();
        log.info("构建股票账单模块: " + billPath);
        this.stockBillStoreService.setConfigPath(billPath);
        List<StockSummaryBO> stockSummaryList = this.stockBillStoreService.loadStockList();
        this.stockBillHoldService.updateStockList(stockSummaryList);
    }

    private StockDataStoreService stockDataStoreService;

    private StockDataHoldService stockDataHoldService;

    private StockMonitorStoreService stockMonitorStoreService;

    private StockMonitorHoldService stockMonitorHoldService;

    private StockBillStoreService stockBillStoreService;

    private StockBillHoldService stockBillHoldService;

    public void setStockDataStoreService(StockDataStoreService stockDataStoreService) {
        this.stockDataStoreService = stockDataStoreService;
    }

    public void setStockDataHoldService(StockDataHoldService stockDataHoldService) {
        this.stockDataHoldService = stockDataHoldService;
    }

    public void setStockMonitorStoreService(StockMonitorStoreService stockMonitorStoreService) {
        this.stockMonitorStoreService = stockMonitorStoreService;
    }

    public void setStockMonitorHoldService(StockMonitorHoldService stockMonitorHoldService) {
        this.stockMonitorHoldService = stockMonitorHoldService;
    }

    public StockBillStoreService getStockBillStoreService() {
        return stockBillStoreService;
    }

    public void setStockBillStoreService(StockBillStoreService stockBillStoreService) {
        this.stockBillStoreService = stockBillStoreService;
    }

    public StockBillHoldService getStockBillHoldService() {
        return stockBillHoldService;
    }

    public void setStockBillHoldService(StockBillHoldService stockBillHoldService) {
        this.stockBillHoldService = stockBillHoldService;
    }
}
