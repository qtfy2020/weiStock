package com.weifly.weistock.module.stockmonitor;

import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.core.monitor.bo.MonitorStatusBO;
import com.weifly.weistock.core.task.UpdateStatus;
import com.weifly.weistock.module.stockmonitor.bo.StockMonitorConfigBO;
import com.weifly.weistock.module.stockmonitor.bo.StockMonitorDayBO;
import com.weifly.weistock.module.stockmonitor.bo.StockMonitorHoldBO;

import java.util.List;

/**
 * 股票监控服务
 *
 * @author weifly
 * @since 2021/6/30
 */
public interface StockMonitorHoldService {

    /**
     * 更新所有股票监控配置
     */
    void updateStockMonitorList(List<StockMonitorConfigBO> stockMonitorConfigList);

    /**
     * 根据股票K线数据，计算股票监控信息
     */
    void calcStockMonitor(StockKLineBO stockKLine, String tradeStockCode);

    /**
     * 获得股票列表
     */
    List<StockMonitorHoldBO> getStockList();

    /**
     * 获得股票监控信息
     */
    StockMonitorHoldBO getStock(String stockCode);

    /**
     * 更新状态信息
     */
    void updateStatus(UpdateStatus status);

    /**
     * 获得状态信息
     */
    MonitorStatusBO getStatus();
}
