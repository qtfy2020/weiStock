package com.weifly.weistock.module.stockmonitor.impl;

import com.weifly.weistock.core.market.StockMarketService;
import com.weifly.weistock.core.task.UpdateStatus;
import com.weifly.weistock.core.task.UpdateTask;
import com.weifly.weistock.module.stockbill.StockBillHoldService;
import com.weifly.weistock.module.stockdata.StockDataHoldService;
import com.weifly.weistock.module.stockmonitor.StockMonitorHoldService;

/**
 * 股票涨跌更新任务
 *
 * @author weifly
 * @since 2021/07/13
 */
public class StockMonitorUpdateTask implements UpdateTask {

    private StockMarketService stockMarketService;

    private StockDataHoldService stockDataHoldService;

    private StockMonitorHoldService stockMonitorHoldService;

    private StockBillHoldService stockBillHoldService;

    public void setStockMarketService(StockMarketService stockMarketService) {
        this.stockMarketService = stockMarketService;
    }

    public void setStockDataHoldService(StockDataHoldService stockDataHoldService) {
        this.stockDataHoldService = stockDataHoldService;
    }

    public void setStockMonitorHoldService(StockMonitorHoldService stockMonitorHoldService) {
        this.stockMonitorHoldService = stockMonitorHoldService;
    }

    public void setStockBillHoldService(StockBillHoldService stockBillHoldService) {
        this.stockBillHoldService = stockBillHoldService;
    }

    @Override
    public void doRunTask(UpdateStatus updateStatus) throws Exception {
        StockMonitorCheckAction checkAction = new StockMonitorCheckAction();
        checkAction.setStockMarketService(this.stockMarketService);
        checkAction.setStockDataHoldService(this.stockDataHoldService);
        checkAction.setStockMonitorHoldService(this.stockMonitorHoldService);
        checkAction.setStockBillHoldService(this.stockBillHoldService);
        checkAction.checkIfNeed(updateStatus);
    }
}
