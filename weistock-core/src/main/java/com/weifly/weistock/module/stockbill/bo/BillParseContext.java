package com.weifly.weistock.module.stockbill.bo;


import com.weifly.weistock.module.stockbill.parse.AbstractBillAnalyzer;

import java.util.List;

/**
 * 股票对账单解析上下文
 *
 * @author weifly
 * @since 2021/05/14
 */
public class BillParseContext {

    /**
     * 选定的分析器
     */
    private AbstractBillAnalyzer billAnalyzer;

    /**
     * 表头列集合
     */
    private List<BillHeadColumn> headColumnList;

    public AbstractBillAnalyzer getBillAnalyzer() {
        return billAnalyzer;
    }

    public void setBillAnalyzer(AbstractBillAnalyzer billAnalyzer) {
        this.billAnalyzer = billAnalyzer;
    }

    public List<BillHeadColumn> getHeadColumnList() {
        return headColumnList;
    }

    public void setHeadColumnList(List<BillHeadColumn> headColumnList) {
        this.headColumnList = headColumnList;
    }
}
