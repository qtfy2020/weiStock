package com.weifly.weistock.module.stockdata;

import com.weifly.weistock.bo.MergeRecordResult;
import com.weifly.weistock.core.market.bo.StockDayBO;
import com.weifly.weistock.core.market.bo.StockDividendBO;
import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.module.stockdata.bo.GetDayListRequest;

import java.util.List;

/**
 * 股票数据服务
 *
 * @author weifly
 * @since 2021/6/9
 */
public interface StockDataHoldService {

    /**
     * 更新所有股票数据
     */
    void updateStockDataList(List<StockKLineBO> stockDataList);

    /**
     * 更新股票数据，并保存到数据文件
     */
    MergeRecordResult updateStockData(StockKLineBO stockData);

    /**
     * 获得股票数据列表
     */
    List<StockKLineBO> getStockDataList();

    /**
     * 获得股票数据
     */
    StockKLineBO getStockData(String stockCode);

    /**
     * 获得天数据列表
     */
    List<StockDayBO> getDayList(GetDayListRequest request);

    /**
     * 更新股票分红信息
     */
    void updateStockDividend(StockDividendBO stockDividend);
}
