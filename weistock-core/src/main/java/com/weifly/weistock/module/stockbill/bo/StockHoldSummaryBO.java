package com.weifly.weistock.module.stockbill.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * 股票持仓统计情况
 *
 * @author weifly
 * @since 2020/12/24
 */
public class StockHoldSummaryBO {

    private String stockCode; // 证券名称, 510300
    private String stockName; // 证券代码, 300ETF
    private Integer holdNumber; // 持仓数量
    private List<StockHoldDayBO> dayList = new ArrayList<>(); // 日期列表

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Integer getHoldNumber() {
        return holdNumber;
    }

    public void setHoldNumber(Integer holdNumber) {
        this.holdNumber = holdNumber;
    }

    public List<StockHoldDayBO> getDayList() {
        return dayList;
    }

    public void setDayList(List<StockHoldDayBO> dayList) {
        this.dayList = dayList;
    }
}
