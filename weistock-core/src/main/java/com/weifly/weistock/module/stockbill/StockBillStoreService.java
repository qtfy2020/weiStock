package com.weifly.weistock.module.stockbill;

import com.weifly.weistock.core.config.ConfigPathSetter;
import com.weifly.weistock.module.stockbill.bo.StockSummaryBO;

import java.util.List;

/**
 * 从xml中加载、保存交易记录
 *
 * @author weifly
 * @since 2020/01/16
 */
public interface StockBillStoreService extends ConfigPathSetter {

    /**
     * 加载股票列表
     */
    List<StockSummaryBO> loadStockList();

    /**
     * 加载股票交易记录
     */
    StockSummaryBO loadStockSummary(String stockCode);

    /**
     * 保存股票交易记录
     */
    void saveStockSummary(StockSummaryBO summaryDto);
}