package com.weifly.weistock.module.stockbill.bo;

import com.weifly.weistock.core.constant.StockBillColumnTypeEnum;

/**
 * 股票对账单头部，某列信息
 *
 * @author weifly
 * @since 2021/05/14
 */
public class BillHeadColumn {

    /**
     * 列类型
     */
    private StockBillColumnTypeEnum columnType;

    /**
     * 列名称
     */
    private String columnName;

    /**
     * 列索引
     */
    private int columnIndex;

    public StockBillColumnTypeEnum getColumnType() {
        return columnType;
    }

    public void setColumnType(StockBillColumnTypeEnum columnType) {
        this.columnType = columnType;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }
}
