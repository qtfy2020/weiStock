package com.weifly.weistock.module.stockbill.parse;

import com.weifly.weistock.core.common.StockException;
import com.weifly.weistock.core.constant.StockBillColumnTypeEnum;
import com.weifly.weistock.module.stockbill.StockBillConverter;
import com.weifly.weistock.module.stockbill.bo.BillHeadColumn;
import com.weifly.weistock.module.stockbill.bo.BillParseContext;
import com.weifly.weistock.module.stockbill.bo.StockRecordBO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * 华宝证券 对账单分析器
 *
 * @author weifly
 * @since 2021/05/14
 */
public class HbzqBillAnalyzer extends AbstractBillAnalyzer {

    @Override
    public List<BillHeadColumn> analyzeHead(List<String> lineList) {
        String headLine = null;
        for (String line : lineList) {
            if (line.startsWith("成交日期")) {
                headLine = line;
                break;
            }
        }
        if (headLine == null) {
            return null;
        }
        String[] columnNames = headLine.split("\\s+");
        if (!columnNames[0].equals("成交日期") || !columnNames[1].equals("成交时间") || !columnNames[2].equals("股东代码")) {
            return null; // 不满足条件
        }
        List<BillHeadColumn> columnList = new ArrayList<>();
        for (int i = 0; i < columnNames.length; i++) {
            this.makeOneColumn(columnList, columnNames[i], i);
        }
        return columnList;
    }

    private void makeOneColumn(List<BillHeadColumn> columnList, String columnName, int columnIndex) {
        StockBillColumnTypeEnum columnType = null;
        if (columnName.equals("成交日期")) {
            columnType = StockBillColumnTypeEnum.DATE;
        } else if (columnName.equals("成交时间")) {
            columnType = StockBillColumnTypeEnum.TIME;
        } else if (columnName.equals("股东代码")) {
            columnType = StockBillColumnTypeEnum.STOCK_HOLDER_CODE;
        } else if (columnName.equals("证券代码")) {
            columnType = StockBillColumnTypeEnum.STOCK_CODE;
        } else if (columnName.equals("证券名称")) {
            columnType = StockBillColumnTypeEnum.STOCK_NAME;
        } else if (columnName.equals("委托类别")) {
            columnType = StockBillColumnTypeEnum.BUSINESS_NAME;
        } else if (columnName.equals("成交价格")) {
            columnType = StockBillColumnTypeEnum.TRADE_PRICE;
        } else if (columnName.equals("成交数量")) {
            columnType = StockBillColumnTypeEnum.TRADE_NUMBER;
        } else if (columnName.equals("发生金额")) {
            // 华宝的“发生金额”实际是“成交金额”
            columnType = StockBillColumnTypeEnum.TRADE_AMOUNT;
        } else if (columnName.equals("剩余金额")) {
            // 不准确
            columnType = StockBillColumnTypeEnum.AFTER_AMOUNT;
        } else if (columnName.equals("佣金")) {
            columnType = StockBillColumnTypeEnum.FEE_SERVICE;
        } else if (columnName.equals("印花税")) {
            columnType = StockBillColumnTypeEnum.FEE_STAMP;
        } else if (columnName.equals("过户费")) {
            columnType = StockBillColumnTypeEnum.FEE_TRANSFER;
        } else if (columnName.equals("成交费")) {
            columnType = StockBillColumnTypeEnum.FEE_CLEAR;
        } else if (columnName.equals("成交编号")) {
            columnType = StockBillColumnTypeEnum.TRANSACTION_NUMBER;
        } else if (columnName.equals("委托编号")) {
            columnType = StockBillColumnTypeEnum.ENTRUST_CODE;
        } else {
            throw new StockException("不支持的列名：" + columnName);
        }
        BillHeadColumn headColumn = new BillHeadColumn();
        headColumn.setColumnType(columnType);
        headColumn.setColumnName(columnName);
        headColumn.setColumnIndex(columnIndex);
        columnList.add(headColumn);
    }

    @Override
    public StockRecordBO analyzeOneRecord(BillParseContext context, String line) {
        StockRecordBO recordDto = super.analyzeOneRecord(context, line);
        if (recordDto != null) {
            if (recordDto.getClearAmount() == null) {
                // 计算发生金额
                this.calcClearAmount(recordDto);
            }
        }
        return recordDto;
    }

    // 计算发生金额
    private void calcClearAmount(StockRecordBO recordDto) {
        BigDecimal clearAmount = null;
        if (StockBillConverter.isOpenRecord(recordDto)) {
            // 开仓操作为负值
            clearAmount = new BigDecimal(-recordDto.getTradeAmount());
        } else if (StockBillConverter.isCloseRecord(recordDto)) {
            // 平仓操作为正值
            clearAmount = new BigDecimal(recordDto.getTradeAmount());
        }
        // 减手续费 feeService
        if (recordDto.getFeeService() != null && recordDto.getFeeService() > 0) {
            clearAmount = clearAmount.subtract(new BigDecimal(recordDto.getFeeService()));
        }
        // 减印花税 feeStamp
        if (recordDto.getFeeStamp() != null && recordDto.getFeeStamp() > 0) {
            clearAmount = clearAmount.subtract(new BigDecimal(recordDto.getFeeStamp()));
        }
        // 减过户费 feeTransfer
        if (recordDto.getFeeTransfer() != null && recordDto.getFeeTransfer() > 0) {
            clearAmount = clearAmount.subtract(new BigDecimal(recordDto.getFeeTransfer()));
        }
        // 附加费 feeExtra
        if (recordDto.getFeeExtra() != null && recordDto.getFeeExtra() > 0) {
            clearAmount = clearAmount.subtract(new BigDecimal(recordDto.getFeeExtra()));
        }
        // 交易所清算费 feeClear
        if (recordDto.getFeeClear() != null && recordDto.getFeeClear() > 0) {
            clearAmount = clearAmount.subtract(new BigDecimal(recordDto.getFeeClear()));
        }
        recordDto.setClearAmount(clearAmount.setScale(2, RoundingMode.HALF_UP).doubleValue());
    }
}
