package com.weifly.weistock.core.market.impl;

import com.weifly.weistock.core.market.bo.StockDayBO;
import com.weifly.weistock.core.util.WeistockUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * 新浪股票工具类
 *
 * @author weifly
 * @since 2021/7/5
 */
public class SinaMarketUtils {

    /**
     * 转换为列表
     */
    public static List convertToDataList(String jsonData) {
        if (StringUtils.isBlank(jsonData)) {
            return null;
        }
        int startIdx = jsonData.indexOf("(");
        if (startIdx == -1) {
            return null;
        }
        int endIdx = jsonData.indexOf(")", startIdx + 1);
        if (endIdx == -1) {
            return null;
        }
        String targetStr = jsonData.substring(startIdx + 1, endIdx);
        return WeistockUtils.toJsonObject(targetStr, List.class);
    }

    /**
     * 排除开盘价为0的记录
     */
    public static void removeZeroRecord(LinkedList<StockDayBO> dayList) {
        while (true) {
            if (dayList.isEmpty()) {
                break;
            }
            if (dayList.getFirst().getOpen() == 0) {
                dayList.removeFirst();
            } else {
                break;
            }
        }
    }
}
