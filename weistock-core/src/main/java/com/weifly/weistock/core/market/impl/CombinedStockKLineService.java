package com.weifly.weistock.core.market.impl;

import com.weifly.weistock.core.constant.StockCatalogEnum;
import com.weifly.weistock.core.market.StockKLineService;
import com.weifly.weistock.core.market.bo.StockKLineBO;

import java.util.Map;

/**
 * 组合多种 KLine 的获取
 *
 * @author weifly
 * @since 2021/7/5
 */
public class CombinedStockKLineService implements StockKLineService {

    private Map<StockCatalogEnum, StockKLineService> kLineServiceMap;

    private StockKLineService defaultKLineService;

    public void setkLineServiceMap(Map<StockCatalogEnum, StockKLineService> kLineServiceMap) {
        this.kLineServiceMap = kLineServiceMap;
    }

    public void setDefaultKLineService(StockKLineService defaultKLineService) {
        this.defaultKLineService = defaultKLineService;
    }

    @Override
    public StockKLineBO loadStockKLine(String stockCode) {
        StockCatalogEnum catalogEnum = StockCatalogEnum.getByFullCode(stockCode);
        StockKLineService service = this.kLineServiceMap.get(catalogEnum);
        if (service == null) {
            service = this.defaultKLineService;
        }
        return service.loadStockKLine(stockCode);
    }
}
