package com.weifly.weistock.core.market.bo;

import com.weifly.weistock.core.constant.StockCatalogEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * 某股票K线数据集合
 *
 * @author weifly
 * @since 2021/6/9
 */
public class StockKLineBO {

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 股票分类
     */
    private StockCatalogEnum stockCatalog;

    /**
     * 分红派息列表
     */
    private List<StockDividendBO> dividendList = new ArrayList<>();

    /**
     * 记录列表
     */
    private List<StockDayBO> dayList = new ArrayList<>();

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public StockCatalogEnum getStockCatalog() {
        return stockCatalog;
    }

    public void setStockCatalog(StockCatalogEnum stockCatalog) {
        this.stockCatalog = stockCatalog;
    }

    public List<StockDividendBO> getDividendList() {
        return dividendList;
    }

    public void setDividendList(List<StockDividendBO> dividendList) {
        this.dividendList = dividendList;
    }

    public List<StockDayBO> getDayList() {
        return dayList;
    }

    public void setDayList(List<StockDayBO> dayList) {
        this.dayList = dayList;
    }
}
