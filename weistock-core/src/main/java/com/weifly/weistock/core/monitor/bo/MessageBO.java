package com.weifly.weistock.core.monitor.bo;

/**
 * 日志信息
 *
 * @author weifly
 * @since 2019/8/14
 */
public class MessageBO {

    private String time; // 时间
    private String message; // 消息

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
