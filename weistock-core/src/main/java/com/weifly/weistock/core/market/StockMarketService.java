package com.weifly.weistock.core.market;

/**
 * 股票行情服务
 *
 * @author weifly
 * @since 2019/2/3
 */
public interface StockMarketService {

    /**
     * 获得股票价格信息
     * @param stockCode
     * @return
     */
    StockPriceDto getStockPrice(String stockCode);

    /**
     * 获得股票价格信息
     *
     * @param fullCode
     */
    public StockPriceDto getStockPriceByFullCode(String fullCode);
}
