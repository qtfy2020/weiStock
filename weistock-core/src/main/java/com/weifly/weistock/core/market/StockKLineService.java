package com.weifly.weistock.core.market;

import com.weifly.weistock.core.market.bo.StockKLineBO;

/**
 * 股票K线服务
 *
 * @author weifly
 * @since 2021/6/9
 */
public interface StockKLineService {

    /**
     * 加载股票K线数据，最多加载1000条
     *
     * @param stockCode 包含前缀的股票代码，例如sh000001
     */
    StockKLineBO loadStockKLine(String stockCode);
}
