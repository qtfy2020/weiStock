package com.weifly.weistock.core.market.bo;

/**
 * 某天的股票日数据
 *
 * @author weifly
 * @since 2021/6/9
 */
public class StockDayBO {

    /**
     * 日期 20200731
     */
    private String day;

    /**
     * 开盘价
     */
    private double open;

    /**
     * 最高价
     */
    private double high;

    /**
     * 最低价
     */
    private double low;

    /**
     * 收盘价
     */
    private double close;

    /**
     * 前复权：开盘价
     */
    private Double front_open;

    /**
     * 前复权：最高价
     */
    private Double front_high;

    /**
     * 前复权：最低价
     */
    private Double front_low;

    /**
     * 前复权：收盘价
     */
    private Double front_close;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public Double getFront_open() {
        return front_open;
    }

    public void setFront_open(Double front_open) {
        this.front_open = front_open;
    }

    public Double getFront_high() {
        return front_high;
    }

    public void setFront_high(Double front_high) {
        this.front_high = front_high;
    }

    public Double getFront_low() {
        return front_low;
    }

    public void setFront_low(Double front_low) {
        this.front_low = front_low;
    }

    public Double getFront_close() {
        return front_close;
    }

    public void setFront_close(Double front_close) {
        this.front_close = front_close;
    }
}
