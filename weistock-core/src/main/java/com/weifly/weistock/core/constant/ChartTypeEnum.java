package com.weifly.weistock.core.constant;

/**
 * chart类型
 *
 * @author weifly
 * @since 2021/1/20
 */
public enum ChartTypeEnum {

    DAY("day", "日K线"),
    WEEK("week", "周K线"),
    MONTH("month", "月K线");

    private String value;

    private String desc;

    ChartTypeEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }
}
