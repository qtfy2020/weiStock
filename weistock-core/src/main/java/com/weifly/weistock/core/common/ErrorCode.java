package com.weifly.weistock.core.common;

/**
 * 错误编码
 *
 * @author weifly
 * @since 2019/1/31
 */
public class ErrorCode {

    public static final String RESULT_SUCC = "200";
    public static final String RESULT_PARAM_ERROR = "E1000";
    public static final String RESULT_SYS_ERROR = "E9999";
}
