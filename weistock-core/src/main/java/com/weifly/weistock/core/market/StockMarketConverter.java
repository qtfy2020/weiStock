package com.weifly.weistock.core.market;

import com.weifly.weistock.core.market.bo.StockKLineBO;

/**
 * converter
 *
 * @author weifly
 * Create at 2021/7/13
 */
public class StockMarketConverter {

    public static StockKLineBO createStockKLine(StockPriceDto stockPrice) {
        StockKLineBO stockKLineBO = new StockKLineBO();
        stockKLineBO.setStockCode(stockPrice.getCode());
        stockKLineBO.setStockName(stockPrice.getName());
        stockKLineBO.setStockCatalog(stockPrice.getStockCatalog());
        return stockKLineBO;
    }
}
