package com.weifly.weistock.core.monitor.bo;

/**
 * 监控状态
 *
 * @author weifly
 * @since 2019/8/14
 */
public class MonitorStatusBO {

    private int status; // 状态
    private String date; // 更新日期
    private String time; // 更新时间

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
