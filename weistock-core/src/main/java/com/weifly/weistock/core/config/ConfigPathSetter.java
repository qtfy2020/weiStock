package com.weifly.weistock.core.config;

/**
 * 配置路径接口
 *
 * @author weifly
 * @since 2021/6/11
 */
public interface ConfigPathSetter {

    /**
     * 设置配置文件路径
     */
    void setConfigPath(String configPath);
}