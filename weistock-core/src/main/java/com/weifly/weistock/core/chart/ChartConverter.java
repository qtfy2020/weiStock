package com.weifly.weistock.core.chart;

import com.weifly.weistock.core.chart.bo.ChartConfigBO;
import com.weifly.weistock.core.constant.ChartTypeEnum;
import com.weifly.weistock.core.util.DateUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * converter
 *
 * @author weifly
 * Create at 2021/01/20
 */
public class ChartConverter {

    /**
     * chart配置
     */
    public static ChartConfigBO getChartConfig(HttpServletRequest request) {
        ChartConfigBO chartConfig = new ChartConfigBO();

        String type = request.getParameter("type");
        String day = request.getParameter("day");
        Calendar dayLimitCalc = Calendar.getInstance();
        if (ChartTypeEnum.WEEK.getValue().equals(type)) {
            chartConfig.setType(ChartTypeEnum.WEEK);
            dayLimitCalc.add(Calendar.MONTH, -6); // 周K线至少6个月数据
        } else if (ChartTypeEnum.MONTH.getValue().equals(type)) {
            chartConfig.setType(ChartTypeEnum.MONTH);
            dayLimitCalc.add(Calendar.MONTH, -12); // 月K线至少12个月数据
        } else {
            chartConfig.setType(ChartTypeEnum.DAY);
            dayLimitCalc.add(Calendar.MONTH, -1); // 日K线至少1个月数据
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.FORMATTER_DATE);
        String dayLimit = sdf.format(dayLimitCalc.getTime());
        if (StringUtils.isBlank(day)) {
            day = dayLimit;
        } else if (day.compareTo(dayLimit) > 0) {
            day = dayLimit;
        }
        chartConfig.setDay(day);

        // 叠加品种
        List<String> overList = Collections.emptyList();
        String[] overs = request.getParameterValues("over");
        if (overs != null && overs.length > 0) {
            overList = Arrays.asList(overs);
        }
        chartConfig.setOverList(overList);

        return chartConfig;
    }
}
