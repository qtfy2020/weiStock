package com.weifly.weistock.core.market.impl;

import com.weifly.weistock.core.market.StockKLineService;
import com.weifly.weistock.core.market.StockMarketConverter;
import com.weifly.weistock.core.market.StockPriceDto;
import com.weifly.weistock.core.market.bo.StockDayBO;
import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.core.util.DateUtils;
import com.weifly.weistock.core.util.HttpRequestUtils;
import com.weifly.weistock.core.util.WeistockUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 新浪股票K线
 *
 * @author weifly
 * @since 2021/6/9
 */
public class SinaStockKLineService implements StockKLineService {

    private Logger log = LoggerFactory.getLogger(SinaStockKLineService.class);

    private SinaStockMarketService stockMarketService = new SinaStockMarketService();

    @Override
    public StockKLineBO loadStockKLine(String stockCode) {
        // http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData?symbol=sz002095&ma=no&datalen=1023&scale=240
        String url = "http://money.finance.sina.com.cn/quotes_service/api/json_v2.php/CN_MarketData.getKLineData";
        Map<String, String> params = new LinkedHashMap<>();
        params.put("symbol", stockCode);
        params.put("ma", "no");
        params.put("datalen", "1000");
        params.put("scale", "240"); // 240分钟，一天的交易时长
        String klineDataStr = HttpRequestUtils.sendGet(url, params);
        if (StringUtils.isBlank(klineDataStr)) {
            log.info("无法加载K线数据，stockCode=" + stockCode);
            return null;
        }

        StockPriceDto stockPriceDto = this.stockMarketService.getStockPriceByFullCode(stockCode);
        StockKLineBO stockKLine = StockMarketConverter.createStockKLine(stockPriceDto);
        List dayInfoList = WeistockUtils.toJsonObject(klineDataStr, List.class);
        if (dayInfoList != null) {
            SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdfTo = new SimpleDateFormat(DateUtils.FORMATTER_DATE);
            for (int i = 0; i < dayInfoList.size(); i++) {
                Map dayInfo = (Map) dayInfoList.get(i);
                StockDayBO stockDay = new StockDayBO();
                try {
                    String day = (String) dayInfo.get("day");
                    stockDay.setDay(sdfTo.format(sdfFrom.parse(day)));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
                // 开盘价
                String openStr = (String) dayInfo.get("open");
                stockDay.setOpen(Double.parseDouble(openStr));
                // 最高价
                String highStr = (String) dayInfo.get("high");
                stockDay.setHigh(Double.parseDouble(highStr));
                // 最低价
                String lowStr = (String) dayInfo.get("low");
                stockDay.setLow(Double.parseDouble(lowStr));
                // 收盘价
                String closeStr = (String) dayInfo.get("close");
                stockDay.setClose(Double.parseDouble(closeStr));
                stockKLine.getDayList().add(stockDay);
            }
        }
        return stockKLine;
    }
}
