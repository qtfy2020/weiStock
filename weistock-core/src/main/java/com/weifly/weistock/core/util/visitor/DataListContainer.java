package com.weifly.weistock.core.util.visitor;

import java.util.List;

/**
 * 数据集合container
 *
 * @author weifly
 * @since 2021/1/22
 */
public interface DataListContainer<E> {

    List<E> getDataList();
}
