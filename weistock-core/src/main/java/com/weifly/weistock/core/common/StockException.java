package com.weifly.weistock.core.common;

/**
 * 系统异常
 *
 * @author weifly
 * @since 2019/8/07
 */
public class StockException extends RuntimeException {

    /**
     * 错误码
     */
    private String code = ErrorCode.RESULT_SYS_ERROR;

    public StockException() {
        super();
    }

    public StockException(String message) {
        super(message);
    }

    public StockException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
