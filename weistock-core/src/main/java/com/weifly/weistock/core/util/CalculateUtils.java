package com.weifly.weistock.core.util;

import com.weifly.weistock.bo.DiffRateBO;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 计算工具类
 *
 * @author weifly
 * @since 2021/7/1
 */
public class CalculateUtils {

    /**
     * 计算差值、比率
     *
     * @param baseValue  基准值
     * @param afterValue 变动值
     */
    public static DiffRateBO calcDiffAndRate(double baseValue, double afterValue) {
        double diff = WeistockUtils.subtract(afterValue, baseValue);
        double rate = new BigDecimal(WeistockUtils.multi(diff, 100)).divide(new BigDecimal(baseValue), 2, RoundingMode.HALF_UP).doubleValue();
        DiffRateBO bo = new DiffRateBO();
        bo.setDiff(diff);
        bo.setRate(rate);
        return bo;
    }

    /**
     * 计算百分比
     *
     * @param num1
     * @param num2 总数
     */
    public static double computePercent(int num1, int num2) {
        return computePercent(num1, new BigDecimal(num2));
    }

    public static double computePercent(int num1, BigDecimal num2) {
        return new BigDecimal(num1 * 100).divide(num2, 2, RoundingMode.HALF_UP).doubleValue();
    }
}
