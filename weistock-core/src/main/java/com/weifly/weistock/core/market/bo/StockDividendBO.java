package com.weifly.weistock.core.market.bo;

/**
 * 股息、红利信息
 *
 * @author weifly
 * @since 2021/6/9
 */
public class StockDividendBO {

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 日期 20200731
     */
    private String day;

    /**
     * 现金分红金额，10股派3.26元
     */
    private Double dividendAmount;

    /**
     * 股票分红数量，10股转增3股
     */
    private Integer dividendStock;

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Double getDividendAmount() {
        return dividendAmount;
    }

    public void setDividendAmount(Double dividendAmount) {
        this.dividendAmount = dividendAmount;
    }

    public Integer getDividendStock() {
        return dividendStock;
    }

    public void setDividendStock(Integer dividendStock) {
        this.dividendStock = dividendStock;
    }
}
