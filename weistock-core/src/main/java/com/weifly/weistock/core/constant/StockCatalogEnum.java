package com.weifly.weistock.core.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 股票分类
 *
 * @author weifly
 * @since 2021/1/19
 */
public enum StockCatalogEnum {

    SH_STOCK("上海股票", "1", "sh", 0.01),
    SH_FUND("上海基金", "1", "sh", 0.001),
    SH_INDEX("上海指数", "1", "sh", 0.01),
    SZ_STOCK("深圳股票", "0", "sz", 0.01),
    SZ_FUND("深圳基金", "0", "sz", 0.001),
    SZ_INDEX("深圳指数", "0", "sz", 0.01),
    HK_STOCK("香港股票", null, "rt_", null),
    HF_STOCK("全球大宗", null, "hf_", null);

    /**
     * 已知的股票分类
     */
    public static Map<String, StockCatalogEnum> stockCatalogMap = new HashMap<>();

    static {
        stockCatalogMap.put("sh000001", SH_INDEX); // 上证指数
        stockCatalogMap.put("sh000300", SH_INDEX); // 沪深300
        stockCatalogMap.put("sz399001", SZ_INDEX); // 深证成指
        stockCatalogMap.put("sz399006", SZ_INDEX); // 创业板指
    }

    private String desc;

    /**
     * 交易所ID
     */
    private String exchangeId = null;

    /**
     * 交易所前缀
     */
    private String exchangePrefix = null;

    /**
     * 最小变动价格，股票为0.01，基金为0.001
     */
    private Double minStepPrice = null;

    StockCatalogEnum(String desc, String exchangeId, String exchangePrefix, Double minStepPrice) {
        this.desc = desc;
        this.exchangeId = exchangeId;
        this.exchangePrefix = exchangePrefix;
        this.minStepPrice = minStepPrice;
    }

    public String getDesc() {
        return desc;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public String getExchangePrefix() {
        return exchangePrefix;
    }

    public Double getMinStepPrice() {
        return minStepPrice;
    }

    /**
     * 获取股票分类
     *
     * @param stockCode 带前缀的股票代码
     */
    public static StockCatalogEnum getByFullCode(String stockCode) {
        if (stockCatalogMap.containsKey(stockCode)) {
            return stockCatalogMap.get(stockCode);
        }
        if (stockCode.startsWith("sh6")) {
            return StockCatalogEnum.SH_STOCK; // 上海股票
        } else if (stockCode.startsWith("sh5")) {
            return StockCatalogEnum.SH_FUND; // 上海基金
        } else if (stockCode.startsWith("sh000")) {
            return StockCatalogEnum.SH_INDEX; // 上海指数
        } else if (stockCode.startsWith("sz1")) {
            return StockCatalogEnum.SZ_FUND; // 深圳基金
        } else if (stockCode.startsWith("sz0") || stockCode.startsWith("sz3")) {
            return StockCatalogEnum.SZ_STOCK; // 深圳股票
        } else if (stockCode.startsWith("sz399")) {
            return StockCatalogEnum.SZ_INDEX; // 深圳指数
        } else if (stockCode.startsWith("rt_hk")) {
            return StockCatalogEnum.HK_STOCK; // 香港股票
        } else if (stockCode.startsWith("hf_")) {
            return StockCatalogEnum.HF_STOCK; // 全球大宗
        } else {
            return null;
        }
    }

    /**
     * 获取股票分类
     *
     * @param stockCode 不带前缀的，仅包含数字的代码
     */
    public static StockCatalogEnum getByNumberCode(String stockCode) {
        if (stockCode.startsWith("6")) {
            return StockCatalogEnum.SH_STOCK; // 上海股票
        } else if (stockCode.startsWith("5")) {
            return StockCatalogEnum.SH_FUND; // 上海基金
        } else if (stockCode.startsWith("1")) {
            return StockCatalogEnum.SZ_FUND; // 深圳基金
        } else if (stockCode.startsWith("0") || stockCode.startsWith("3")) {
            return StockCatalogEnum.SZ_STOCK; // 深圳股票
        } else if (stockCode.startsWith("hk")) {
            return StockCatalogEnum.HK_STOCK; // 香港股票
        } else {
            return null;
        }
    }

    /**
     * 根据名称获取股票分类
     */
    public static StockCatalogEnum getByName(String name) {
        for (StockCatalogEnum oneEnum : StockCatalogEnum.values()) {
            if (oneEnum.name().equals(name)) {
                return oneEnum;
            }
        }
        return null;
    }
}
