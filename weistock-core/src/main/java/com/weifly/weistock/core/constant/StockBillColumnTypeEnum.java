package com.weifly.weistock.core.constant;

/**
 * 原始对账单列类型
 *
 * @author weifly
 * @since 2021/2/7
 */
public enum StockBillColumnTypeEnum {

    DATE, // 必有: 发生日期, 20191224
    TIME, // 必有: 成交时间, 09:42:51
    BUSINESS_NAME, // 必有: 业务名称 证券买入
    STOCK_CODE, // 必有: 证券代码, 510300
    STOCK_NAME, // 必有: 证券名称, 300ETF
    TRADE_PRICE, // 必有: 成交价格, 3.9710
    TRADE_NUMBER, // 必有: 成交数量, 200.00
    TRADE_AMOUNT, // 成交金额, 794.20
    AFTER_NUMBER, // 股份余额, 2200.00
    FEE_SERVICE, // 必有: 手续费
    FEE_STAMP, // 必有: 印花税
    FEE_TRANSFER, // 必有: 过户费
    FEE_EXTRA, // 附加费
    FEE_CLEAR, // 必有: 交易所清算费
    CLEAR_AMOUNT, // 必有: 发生金额
    AFTER_AMOUNT, // 必有: 资金本次余额
    ENTRUST_CODE, // 必有: 委托编号

    STOCK_HOLDER_CODE, // 股东代码
    TRANSACTION_NUMBER, // 成交编号、流水号
    FUND_ACCOUNT, // 资金帐号
    CURRENCY, // 币种
    REMARK; // 备注

    /**
     * 根据columnName查找columnType
     */
    public static StockBillColumnTypeEnum findColumnType(String columnName) {
        if (columnName.equals("发生日期") || columnName.equals("成交日期")) {
            return DATE;
        } else if (columnName.equals("成交时间")) {
            return TIME;
        } else if (columnName.equals("业务名称") || columnName.equals("委托类别")) {
            return BUSINESS_NAME;
        } else if (columnName.equals("证券代码")) {
            return STOCK_CODE;
        } else if (columnName.equals("证券名称")) {
            return STOCK_NAME;
        } else if (columnName.equals("成交价格")) {
            return TRADE_PRICE;
        } else if (columnName.equals("成交数量")) {
            return TRADE_NUMBER;
        } else if (columnName.equals("成交金额")) {
            return TRADE_AMOUNT;
        } else if (columnName.equals("股份余额")) {
            return AFTER_NUMBER;
        } else if (columnName.equals("手续费") || columnName.equals("佣金")) {
            return FEE_SERVICE;
        } else if (columnName.equals("印花税")) {
            return FEE_STAMP;
        } else if (columnName.equals("过户费")) {
            return FEE_TRANSFER;
        } else if (columnName.equals("附加费")) {
            return FEE_EXTRA;
        } else if (columnName.equals("交易所清算费") || columnName.equals("成交费")) {
            return FEE_CLEAR;
        } else if (columnName.equals("发生金额")) {
            return CLEAR_AMOUNT;
        } else if (columnName.equals("资金本次余额") || columnName.equals("剩余金额")) {
            return AFTER_AMOUNT;
        } else if (columnName.equals("委托编号")) {
            return ENTRUST_CODE;
        } else if (columnName.equals("股东代码")) {
            return STOCK_HOLDER_CODE;
        } else if (columnName.equals("成交编号") || columnName.equals("流水号")) {
            return TRANSACTION_NUMBER;
        } else if (columnName.equals("资金帐号")) {
            return FUND_ACCOUNT;
        } else if (columnName.equals("币种")) {
            return CURRENCY;
        } else if (columnName.equals("备注")) {
            return REMARK;
        }
        return null;
    }
}
