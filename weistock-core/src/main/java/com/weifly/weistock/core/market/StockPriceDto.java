package com.weifly.weistock.core.market;

import com.weifly.weistock.core.constant.StockCatalogEnum;

/**
 * 股票价格信息
 *
 * @author weifly
 * @since 2019/2/3
 */
public class StockPriceDto {

    private StockCatalogEnum stockCatalog; // 股票分类
    private String code; // 股票代码
    private String name; // 股票名称
    private String exchangeId; // 交易所ID，0-深圳，1-上海
    private Double minStepPrice; // 最小变动价格，股票为0.01，基金为0.001
    private Double yesterdayClosePrice; // 昨天收盘价
    private Double nowPrice;    // 当前价格
    private Double highPrice;   // 最高价
    private Double lowPrice;    // 最低价
    private String priceDate;   // 价格产生日期
    private String priceTime;   // 价格产生时间
    private Long updateTime; // 更新时间
    private Double downPrice; // 跌停价
    private Double upPrice; // 涨停价

    public StockCatalogEnum getStockCatalog() {
        return stockCatalog;
    }

    public void setStockCatalog(StockCatalogEnum stockCatalog) {
        this.stockCatalog = stockCatalog;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId;
    }

    public Double getMinStepPrice() {
        return minStepPrice;
    }

    public void setMinStepPrice(Double minStepPrice) {
        this.minStepPrice = minStepPrice;
    }

    public Double getYesterdayClosePrice() {
        return yesterdayClosePrice;
    }

    public void setYesterdayClosePrice(Double yesterdayClosePrice) {
        this.yesterdayClosePrice = yesterdayClosePrice;
    }

    public Double getNowPrice() {
        return nowPrice;
    }

    public void setNowPrice(Double nowPrice) {
        this.nowPrice = nowPrice;
    }

    public Double getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(Double highPrice) {
        this.highPrice = highPrice;
    }

    public Double getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(Double lowPrice) {
        this.lowPrice = lowPrice;
    }

    public String getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate;
    }

    public String getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(String priceTime) {
        this.priceTime = priceTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Double getDownPrice() {
        return downPrice;
    }

    public void setDownPrice(Double downPrice) {
        this.downPrice = downPrice;
    }

    public Double getUpPrice() {
        return upPrice;
    }

    public void setUpPrice(Double upPrice) {
        this.upPrice = upPrice;
    }
}
