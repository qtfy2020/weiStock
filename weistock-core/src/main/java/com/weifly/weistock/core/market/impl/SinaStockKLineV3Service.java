package com.weifly.weistock.core.market.impl;

import com.weifly.weistock.core.common.StockException;
import com.weifly.weistock.core.market.StockKLineService;
import com.weifly.weistock.core.market.StockMarketConverter;
import com.weifly.weistock.core.market.StockPriceDto;
import com.weifly.weistock.core.market.bo.StockDayBO;
import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.core.util.DateUtils;
import com.weifly.weistock.core.util.HttpRequestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 新浪股票K线 V3 黄金现货 XAU
 *
 * @author weifly
 * @since 2021/7/5
 */
public class SinaStockKLineV3Service implements StockKLineService {

    // http://stock2.finance.sina.com.cn/futures/api/jsonp.php/$cb/GlobalFuturesService.getGlobalFuturesDailyKLine?_=$rn&source=web&symbol=XAU
    public static final String URL_STOCK_DATA_1 = "http://stock2.finance.sina.com.cn/futures/api/jsonp.php/$cb/GlobalFuturesService.getGlobalFuturesDailyKLine?_=$rn&source=web&symbol=";

    private Logger log = LoggerFactory.getLogger(SinaStockKLineV3Service.class);

    private SinaStockMarketService stockMarketService = new SinaStockMarketService();

    @Override
    public StockKLineBO loadStockKLine(String stockCode) {
        String symbol = this.getSymbol(stockCode);
        String fullUrl = URL_STOCK_DATA_1 + symbol;
        String stockDataStr = HttpRequestUtils.sendGet(fullUrl, "GBK");
        List dayInfoList = SinaMarketUtils.convertToDataList(stockDataStr);
        if (dayInfoList == null || dayInfoList.isEmpty()) {
            return null;
        }

        // 解析day集合
        LinkedList<StockDayBO> dayList = new LinkedList<>();
        SimpleDateFormat sdfFrom = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfTo = new SimpleDateFormat(DateUtils.FORMATTER_DATE);
        for (int i = 0; i < dayInfoList.size(); i++) {
            Map dayInfo = (Map) dayInfoList.get(i);
            StockDayBO dayBO = new StockDayBO();
            try {
                String day = (String) dayInfo.get("date");
                dayBO.setDay(sdfTo.format(sdfFrom.parse(day)));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            // 开盘价
            String openStr = (String) dayInfo.get("open");
            dayBO.setOpen(Double.parseDouble(openStr));
            // 最高价
            String highStr = (String) dayInfo.get("high");
            dayBO.setHigh(Double.parseDouble(highStr));
            // 最低价
            String lowStr = (String) dayInfo.get("low");
            dayBO.setLow(Double.parseDouble(lowStr));
            // 收盘价
            String closeStr = (String) dayInfo.get("close");
            dayBO.setClose(Double.parseDouble(closeStr));
            dayList.add(dayBO);
        }
        // 排除开盘价为0的记录
        SinaMarketUtils.removeZeroRecord(dayList);

        StockPriceDto stockPriceDto = this.stockMarketService.getStockPriceByFullCode(stockCode);
        StockKLineBO stockKLine = StockMarketConverter.createStockKLine(stockPriceDto);
        stockKLine.getDayList().addAll(dayList);
        return stockKLine;
    }

    private String getSymbol(String stockCode) {
        if (StringUtils.isBlank(stockCode)) {
            throw new StockException("stockCode不能为空");
        }
        if (!stockCode.startsWith("hf_")) {
            throw new StockException("stockCode前缀应是hf_");
        }
        return stockCode.substring(3);
    }
}
