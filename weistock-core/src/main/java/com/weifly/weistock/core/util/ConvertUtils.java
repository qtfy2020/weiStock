package com.weifly.weistock.core.util;

import com.weifly.weistock.core.common.StockException;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 转换工具类
 *
 * @author weifly
 * @since 2021/1/14
 */
public class ConvertUtils {

    /**
     * 列表转换方法
     *
     * @param items    原列表
     * @param function 方法
     * @param <T>      入参模板
     * @param <R>      出参模板
     * @return 结果列表
     */
    public static <T, R> List<R> convert(List<T> items, Function<T, R> function) {
        if (items == null || items.isEmpty()) {
            return Collections.emptyList();
        }
        return items
                .stream()
                .map(function)
                .collect(Collectors.toList());
    }

    /**
     * 去除空字符
     */
    public static String removeEmptyChar(String str) {
        if (StringUtils.isBlank(str)) {
            return str;
        }
        if (str.contains(" ")) {
            str = str.replaceAll(" ", "");
        }
        return str;
    }

    /**
     * 为空时，抛出异常
     */
    public static void checkEmpty(Object value, String message) {
        if (value == null) {
            throw new StockException(message);
        }
        if (value instanceof String) {
            if (StringUtils.isBlank((String) value)) {
                throw new StockException(message);
            }
        } else if (value instanceof List) {
            if (((List) value).isEmpty()) {
                throw new StockException(message);
            }
        }
    }
}
