package com.weifly.weistock.core.market;

import com.weifly.weistock.core.market.impl.SinaStockMarketService;
import com.weifly.weistock.core.util.WeistockUtils;
import org.junit.Test;

public class SinaStockMarketServiceTest {

    @Test
    public void testGetStockPrice() {
        SinaStockMarketService marketService = new SinaStockMarketService();
        StockPriceDto stockPrice1 = marketService.getStockPrice("510300");
        System.out.println(WeistockUtils.toJsonString(stockPrice1));

        StockPriceDto stockPrice2 = marketService.getStockPriceByFullCode("sh000300");
        System.out.println(WeistockUtils.toJsonString(stockPrice2));
    }

    @Test
    public void testGetStockPrice_HF() {
        SinaStockMarketService marketService = new SinaStockMarketService();
        StockPriceDto stockPrice1 = marketService.getStockPriceByFullCode("hf_XAU");
        System.out.println(WeistockUtils.toJsonString(stockPrice1));
    }

    @Test
    public void testGetStockPrice_399905() {
        SinaStockMarketService marketService = new SinaStockMarketService();
        StockPriceDto stockPrice1 = marketService.getStockPriceByFullCode("sz399905");
        System.out.println(WeistockUtils.toJsonString(stockPrice1));
    }
}
