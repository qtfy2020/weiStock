package com.weifly.weistock.core.util;

import org.junit.Test;

import java.util.Date;

public class DateUtilsTest {

    @Test
    public void testFormatTime() {
        Date date = DateUtils.parseDate(DateUtils.FORMATTER_DATE, "20210231a");
        System.out.println(date);
    }

    @Test
    public void testCheckDateString() {
        System.out.println("false - " + DateUtils.checkDateString("2021"));
        System.out.println("false - " + DateUtils.checkDateString("20210012"));
        System.out.println("false - " + DateUtils.checkDateString("20210100"));
        System.out.println("false - " + DateUtils.checkDateString("20210132"));
        System.out.println("false - " + DateUtils.checkDateString("20210230"));
        System.out.println("true  - " + DateUtils.checkDateString("20210101"));
        System.out.println("true  - " + DateUtils.checkDateString("20210131"));
    }
}
