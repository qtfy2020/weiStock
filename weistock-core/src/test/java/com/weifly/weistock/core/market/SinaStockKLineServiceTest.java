package com.weifly.weistock.core.market;

import com.weifly.weistock.core.market.bo.StockDayBO;
import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.core.market.impl.SinaStockKLineService;
import com.weifly.weistock.core.market.impl.SinaStockKLineV2Service;
import com.weifly.weistock.core.market.impl.SinaStockKLineV3Service;
import com.weifly.weistock.core.util.WeistockUtils;
import org.junit.Test;

public class SinaStockKLineServiceTest {

    @Test
    public void testLoadStockKLine() {
        SinaStockKLineService stockKLineService = new SinaStockKLineService();
        StockKLineBO stockKLineBO = stockKLineService.loadStockKLine("sh510300");
        for (StockDayBO stockDayBO : stockKLineBO.getDayList()) {
            System.out.println(WeistockUtils.toJsonString(stockDayBO));
        }
    }

    @Test
    public void testLoadStockKLineV2() {
        SinaStockKLineV2Service stockKLineV2Service = new SinaStockKLineV2Service();
        StockKLineBO stockKLineBO = stockKLineV2Service.loadStockKLine("sh000300");
        for (StockDayBO stockDayBO : stockKLineBO.getDayList()) {
            System.out.println(WeistockUtils.toJsonString(stockDayBO));
        }
    }

    @Test
    public void testLoadStockKLineV3() {
        SinaStockKLineV3Service stockKLineV3Service = new SinaStockKLineV3Service();
        StockKLineBO stockKLineBO = stockKLineV3Service.loadStockKLine("hf_XAU");
        for (StockDayBO stockDayBO : stockKLineBO.getDayList()) {
            System.out.println(WeistockUtils.toJsonString(stockDayBO));
        }
    }
}
