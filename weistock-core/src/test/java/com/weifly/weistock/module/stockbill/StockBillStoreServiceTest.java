package com.weifly.weistock.module.stockbill;

import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.module.stockbill.bo.StockHoldDayBO;
import com.weifly.weistock.module.stockbill.bo.StockHoldSummaryBO;
import com.weifly.weistock.module.stockbill.bo.StockSummaryBO;
import com.weifly.weistock.module.stockbill.impl.StockBillStoreServiceImpl;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * 测试
 *
 * @author weifly
 * @since 2020/01/16
 */
public class StockBillStoreServiceTest {

    @Test
    public void testLoadStockSummary() throws IOException {
        StockBillStoreServiceImpl stockBillStoreService = new StockBillStoreServiceImpl();
        stockBillStoreService.setConfigPath("c:/weistock/bill");

        List<StockSummaryBO> stockList = stockBillStoreService.loadStockList();
        for (StockSummaryBO stockSummaryBO : stockList) {
            System.out.println(stockSummaryBO.getStockCode());
        }
    }

    @Test
    public void testCalcStockHoldSummary() {
        StockBillStoreServiceImpl stockBillStoreService = new StockBillStoreServiceImpl();
        stockBillStoreService.setConfigPath("c:/weistock/bill");

        StockSummaryBO stockSummaryBO = stockBillStoreService.loadStockSummary("160133");
        StockHoldSummaryBO holdSummaryBO = StockBillConverter.convertToStockHoldSummaryBO(stockSummaryBO);
        for (StockHoldDayBO dayBO : holdSummaryBO.getDayList()) {
            System.out.println(WeistockUtils.toJsonString(dayBO));
        }
    }
}
