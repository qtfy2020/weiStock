package com.weifly.weistock.module.stockdata;

import com.weifly.weistock.core.market.bo.StockDividendBO;
import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.module.stockdata.impl.StockDataHoldServiceImpl;
import com.weifly.weistock.module.stockdata.impl.StockDataStoreServiceImpl;
import org.junit.Test;

import java.util.List;

public class StockDataStoreServiceTest {

    @Test
    public void testLoadStockDataList() {
        StockDataStoreServiceImpl stockDataStoreService = new StockDataStoreServiceImpl();
        stockDataStoreService.setConfigPath("C:\\weistock\\data");
        List<StockKLineBO> stockKLineList = stockDataStoreService.loadStockDataList();
        System.out.println("size: " + stockKLineList.size());
        for (StockKLineBO stockKLineBO : stockKLineList) {
            System.out.println(stockKLineBO.getStockCode());
        }
    }

    @Test
    public void testUpdateStockDividend() {
        StockDataStoreServiceImpl stockDataStoreService = new StockDataStoreServiceImpl();
        stockDataStoreService.setConfigPath("C:\\weistock\\data");

        StockDataHoldServiceImpl stockDataHoldService = new StockDataHoldServiceImpl();
        stockDataHoldService.setStockDataStoreService(stockDataStoreService);

        StockDividendBO stockDividendBO = new StockDividendBO();
        stockDividendBO.setStockCode("sh510300");
        stockDividendBO.setDay("20210118");
        stockDividendBO.setDividendAmount(0.72);

        stockDataHoldService.updateStockDividend(stockDividendBO);
    }
}
