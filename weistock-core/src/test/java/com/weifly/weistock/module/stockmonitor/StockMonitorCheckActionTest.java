package com.weifly.weistock.module.stockmonitor;

import com.weifly.weistock.core.constant.StockCatalogEnum;
import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.core.market.impl.SinaStockMarketService;
import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.module.stockdata.StockDataHoldService;
import com.weifly.weistock.module.stockdata.impl.StockDataHoldServiceImpl;
import com.weifly.weistock.module.stockdata.impl.StockDataStoreServiceImpl;
import com.weifly.weistock.module.stockmonitor.bo.StockMonitorConfigBO;
import com.weifly.weistock.module.stockmonitor.bo.StockMonitorHoldBO;
import com.weifly.weistock.module.stockmonitor.impl.StockMonitorCheckAction;
import com.weifly.weistock.module.stockmonitor.impl.StockMonitorHoldServiceImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StockMonitorCheckActionTest {

    @Test
    public void testCheck() {
        StockDataHoldService stockDataHoldService = this.createStockDataHoldService();
        StockMonitorHoldService stockMonitorHoldService = this.createStockMonitorHoldService();
        StockMonitorCheckAction checkAction = this.createStockMonitorCheckAction(stockDataHoldService, stockMonitorHoldService);

        checkAction.check(StockMonitorCheckAction.CHECK_ALL);

        for (StockMonitorHoldBO holdBO : stockMonitorHoldService.getStockList()) {
            System.out.println(WeistockUtils.toJsonString(holdBO.getStockMonitorStatus()));
        }
    }

    private StockDataHoldService createStockDataHoldService() {
        StockDataStoreServiceImpl stockDataStoreService = new StockDataStoreServiceImpl();
        stockDataStoreService.setConfigPath("C:\\weistock\\data");
        List<StockKLineBO> stockDataList = new ArrayList<>();
        // 加载沪深300
        stockDataList.add(stockDataStoreService.loadStockData("sh000300"));
        // 创建holdService
        StockDataHoldService stockDataHoldService = new StockDataHoldServiceImpl();
        stockDataHoldService.updateStockDataList(stockDataList);
        return stockDataHoldService;
    }

    private StockMonitorHoldService createStockMonitorHoldService() {
        List<StockMonitorConfigBO> stockMonitorConfigList = new ArrayList<>();
        StockMonitorConfigBO configBO = new StockMonitorConfigBO();
        configBO.setStockCode("sh000300");
        configBO.setStockName("沪深300");
        configBO.setStockCatalog(StockCatalogEnum.SH_INDEX);
        stockMonitorConfigList.add(configBO);

        StockMonitorHoldService stockMonitorHoldService = new StockMonitorHoldServiceImpl();
        stockMonitorHoldService.updateStockMonitorList(stockMonitorConfigList);
        return stockMonitorHoldService;
    }

    private StockMonitorCheckAction createStockMonitorCheckAction(StockDataHoldService stockDataHoldService, StockMonitorHoldService stockMonitorHoldService) {
        StockMonitorCheckAction checkAction = new StockMonitorCheckAction();
        checkAction.setStockMarketService(new SinaStockMarketService());
        checkAction.setStockDataHoldService(stockDataHoldService);
        checkAction.setStockMonitorHoldService(stockMonitorHoldService);
        return checkAction;
    }
}
