package com.weifly.weistock.module.stockmonitor;

import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.core.util.WeistockUtils;
import com.weifly.weistock.module.stockdata.impl.StockDataStoreServiceImpl;
import com.weifly.weistock.module.stockmonitor.bo.StockMonitorDayBO;
import com.weifly.weistock.module.stockmonitor.impl.StockMonitorConfigCalculator;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class StockMonitorHoldServiceTest {

    @Test
    public void testCalcStockMonitorConfig() {
        StockDataStoreServiceImpl stockDataStoreService = new StockDataStoreServiceImpl();
        stockDataStoreService.setConfigPath("C:\\weistock\\data");
        StockKLineBO stockKLine = stockDataStoreService.loadStockData("sh000300");

        StockMonitorConfigCalculator calculator = new StockMonitorConfigCalculator(stockKLine.getDayList());
        Map<String, StockMonitorDayBO> map = calculator.calc();
        for (StockMonitorDayBO dayBO : map.values()) {
            System.out.println(WeistockUtils.toJsonString(dayBO));
        }
    }
}
