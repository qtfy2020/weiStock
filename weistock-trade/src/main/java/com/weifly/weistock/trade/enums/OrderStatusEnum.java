package com.weifly.weistock.trade.enums;

/**
 * @author weifly
 * @since 2018/11/16
 */
public enum OrderStatusEnum {
    status_1("待撤"), // 撤单指令还未报到场内
    status_2("正撤"), // 撤单指令已送达公司，正在等待处理，此时不能确定是否已进场
    status_3("部撤"), // 委托指令已成交一部分，未成交部分被撤销
    status_4("已撤"), // 委托指令全部被撤消
    status_5("未报"), // 委托指令还未送入数据处理
    status_6("待报"), // 委托指令还未被数据处理报到场内
    status_7("正报"), // 委托指令已送达公司，正在等待处理，此时不能确定是否已进场
    status_8("已报"), // 已收到下单反馈
    status_9("部成"), // 委托指令部份成交
    status_10("已成"), // 委托指令全部成交
    status_11("撤废"), // 撤单废单，表示撤单指令失败，原因可能是被撤的下单指令已经成交了或场内无法找到这条下单记录
    status_12("废单"); // 交易所反馈的信息，表示该定单无效

    private String status;

    OrderStatusEnum(String status){
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
