package com.weifly.weistock.trade.task;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.core.market.StockPriceDto;
import com.weifly.weistock.trade.monitor.StockMonitorService;
import com.weifly.weistock.trade.trade.SendOrderInfo;
import com.weifly.weistock.trade.trade.StockOrderInfo;

import java.util.List;

/**
 * 股票更新上下文
 *
 * @author weifly
 * @since 2018/10/31
 */
public interface StockUpdateContext {

    /**
     * 获得股票价格
     *
     * @param stockCode
     * @return
     */
    StockPriceDto getStockPrice(String stockCode);

    /**
     * 获得股票委托列表
     *
     * @return
     */
    List<StockOrderInfo> getOrderList();

    /**
     * 委托下单
     * @param sendOrderInfo 下单信息
     * @return 委托信息
     */
    Result sendOrder(SendOrderInfo sendOrderInfo);

    /**
     * 撤销委托
     *
     * @param stockOrderInfo
     * @return
     */
    Result cancelOrder(StockOrderInfo stockOrderInfo);

    /**
     * 获得股票监控服务
     */
    StockMonitorService getStockMonitorService();

    /**
     * 获得停止下单时间，大于此时间时，不再下单
     */
    Long getStopSendOrderTime();
}
