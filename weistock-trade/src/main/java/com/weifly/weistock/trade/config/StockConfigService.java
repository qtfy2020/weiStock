package com.weifly.weistock.trade.config;

/**
 * 股票配置服务
 *
 * @author weifly
 * @since 2018/8/20
 */
public interface StockConfigService {

    /**
     * 设置配置文件路径
     */
    void setConfigPath(String configPath);

    /**
     * 解析股票配置信息
     */
    AllStockConfig parseConfig();

    /**
     * 保存股票配置信息
     */
    void saveConfig(AllStockConfig allStockConfig);
}
