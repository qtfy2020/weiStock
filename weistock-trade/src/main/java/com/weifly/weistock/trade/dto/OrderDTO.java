package com.weifly.weistock.trade.dto;

/**
 * 委托单信息
 *
 * @author weifly
 * @since 2019/8/14
 */
public class OrderDTO {

    private String orderNumber; // 委托编号
    private Double entrustPrice; // 委托价格
    private String orderTime; // 委托时间

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Double getEntrustPrice() {
        return entrustPrice;
    }

    public void setEntrustPrice(Double entrustPrice) {
        this.entrustPrice = entrustPrice;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }
}
