package com.weifly.weistock.trade.trade;

import com.weifly.weistock.trade.enums.BuyOrSellEnum;
import com.weifly.weistock.trade.enums.SendCategory;

/**
 * 委托下单信息
 *
 * @author weifly
 * @since 2018/11/19
 */
public class SendOrderInfo {

    private SendCategory category; // 委托种类
    private String stockCode; // 股票代码
    private Double entrustPrice;   // 委托价格
    private Integer entrustSize;   // 委托数量

    private String stockName; // 股票名称
    private BuyOrSellEnum buyOrSell; // 委托方向，1-买入、2-卖出
    private boolean creditBuy;  // 融资买入

    public SendCategory getCategory() {
        return category;
    }

    public void setCategory(SendCategory category) {
        this.category = category;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Double getEntrustPrice() {
        return entrustPrice;
    }

    public void setEntrustPrice(Double entrustPrice) {
        this.entrustPrice = entrustPrice;
    }

    public Integer getEntrustSize() {
        return entrustSize;
    }

    public void setEntrustSize(Integer entrustSize) {
        this.entrustSize = entrustSize;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public BuyOrSellEnum getBuyOrSell() {
        return buyOrSell;
    }

    public void setBuyOrSell(BuyOrSellEnum buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public boolean isCreditBuy() {
        return creditBuy;
    }

    public void setCreditBuy(boolean creditBuy) {
        this.creditBuy = creditBuy;
    }
}
