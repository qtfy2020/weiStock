package com.weifly.weistock.trade.dto;

import com.weifly.weistock.trade.dto.OrderDTO;

/**
 * 股票信息
 *
 * @author weifly
 * @since 2019/8/14
 */
public class StockDTO {

    private String stockCode;
    private String stockName;
    private Double newestPrice;
    private Double basePrice;
    private String lastOperation; // 最后一次操作类型，买或卖
    private OrderDTO buyInfo;
    private OrderDTO sellInfo;

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Double getNewestPrice() {
        return newestPrice;
    }

    public void setNewestPrice(Double newestPrice) {
        this.newestPrice = newestPrice;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public String getLastOperation() {
        return lastOperation;
    }

    public void setLastOperation(String lastOperation) {
        this.lastOperation = lastOperation;
    }

    public OrderDTO getBuyInfo() {
        return buyInfo;
    }

    public void setBuyInfo(OrderDTO buyInfo) {
        this.buyInfo = buyInfo;
    }

    public OrderDTO getSellInfo() {
        return sellInfo;
    }

    public void setSellInfo(OrderDTO sellInfo) {
        this.sellInfo = sellInfo;
    }
}
