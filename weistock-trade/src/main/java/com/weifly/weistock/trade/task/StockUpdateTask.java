package com.weifly.weistock.trade.task;

import com.weifly.weistock.core.task.AbstractUpdateService;
import com.weifly.weistock.core.task.UpdateTask;
import com.weifly.weistock.core.market.StockMarketService;
import com.weifly.weistock.trade.bo.StockMonitorBO;
import com.weifly.weistock.trade.monitor.StockMonitorService;
import com.weifly.weistock.trade.trade.StockTradeService;
import com.weifly.weistock.core.task.UpdateStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 股票更新任务
 *
 * @author weifly
 * @since 2019/3/3
 */
public class StockUpdateTask implements UpdateTask {

    private static final int TIME_9HOUR_0MINUTE = 9*3600*1000 + 0*60*1000; // 09:00:00
    private static final int TIME_9HOUR_15MINUTE = 9*3600*1000 + 15*60*1000; // 09:15:00
    private static final int TIME_15HOUR_3MINUTE = 15*3600*1000 + 3*60*1000; // 15:03:00

    private Logger logger = LoggerFactory.getLogger(StockUpdateTask.class);

    private StockMonitorService stockMonitorService;
    private StockTradeService stockTradeService;
    private StockMarketService stockMarketService;

    public void setStockMonitorService(StockMonitorService stockMonitorService) {
        this.stockMonitorService = stockMonitorService;
    }

    public void setStockTradeService(StockTradeService stockTradeService) {
        this.stockTradeService = stockTradeService;
    }

    public void setStockMarketService(StockMarketService stockMarketService) {
        this.stockMarketService = stockMarketService;
    }

    public void doRunTask(UpdateStatus updateStatus) throws Exception {
        // 更新检测时间
        this.stockMonitorService.updateStatus(updateStatus);

        // 判断是否执行
        if(updateStatus.getStatus()==AbstractUpdateService.STATUS_RUN){
            int todayTime = updateStatus.getCheckHour()*3600000 + updateStatus.getCheckMinute()*60000 + updateStatus.getCheckSecond()*1000;

            if(TIME_9HOUR_0MINUTE<=todayTime && todayTime<TIME_9HOUR_15MINUTE){
                // 清空前一天的下单记录
                this.updateForClear();
            }
            if(TIME_9HOUR_15MINUTE<=todayTime && todayTime<=TIME_15HOUR_3MINUTE){
                // 监控交易情况
                this.updateForTrade();
            }
        }
    }

    private void updateForClear(){
        this.stockMonitorService.writeMessage("清空下单信息");
        this.stockMonitorService.clearStockMonitorInfo();
    }

    private void updateForTrade(){
        this.stockMonitorService.writeMessage("开始更新股票...");
        List<StockMonitorBO> allStocks = this.stockMonitorService.getStockList();
        if(allStocks==null || allStocks.isEmpty()){
            // 没有股票
            return;
        }

        StockCheckAction action = new StockCheckAction();
        StockUpdateContextImpl ctx = new StockUpdateContextImpl();
        ctx.setStockUpdateService(this.stockMarketService);
        ctx.setStockTradeService(this.stockTradeService);
        ctx.setStockMonitorService(this.stockMonitorService);
        // 14:58:00 后停止下单
        ctx.setStopSendOrderTime(createStopSendOrderTime(14, 58));

        for(StockMonitorBO stockInfo : allStocks){
            if(Boolean.TRUE.equals(stockInfo.getStockConfig().getOpen())){
                action.check(stockInfo, ctx);
            }
        }
    }

    public static long createStopSendOrderTime(int hour, int minute){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public static String formatStopSendOrderTime(long stopSendOrderTime){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(new Date(stopSendOrderTime));
    }
}
