package com.weifly.weistock.trade.trade;

import com.weifly.weistock.trade.TradeTestUtils;
import com.weifly.weistock.trade.monitor.impl.StockMonitorServiceImpl;
import com.weifly.weistock.trade.trade.tradeX.TradeXStockTradeService;
import org.junit.Test;

import java.util.List;

public class TestTradeXStockTradeService {

    @Test
    public void testGetOrderList(){
        StockTradeService stockTradeService = new TradeXStockTradeService();

        StockMonitorServiceImpl stockMonitorService = new StockMonitorServiceImpl();
        stockMonitorService.setStockTradeService(stockTradeService);
        stockTradeService.setStockMonitorService(stockMonitorService);

        stockMonitorService.updateAccountConfig(TradeTestUtils.createAccountConfig(), false);

        // 第一次获得委托列表
        List<StockOrderInfo> orderList1 = stockTradeService.getOrderList();
        System.out.println("first getOrderList, order size: " + orderList1.size());
        stockTradeService.logoff();
    }
}
