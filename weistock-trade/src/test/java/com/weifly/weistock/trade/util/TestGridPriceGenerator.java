package com.weifly.weistock.trade.util;

import com.weifly.weistock.core.util.grid.GridPriceGenerator;
import com.weifly.weistock.core.util.grid.GridPricePoint;
import org.junit.Test;

import java.util.List;

/**
 * 测试生成网格价格列表
 *
 * @author weifly
 * @since 2019/8/13
 */
public class TestGridPriceGenerator {

    @Test
    public void testGenerateGridPriceList(){
        GridPriceGenerator generator = new GridPriceGenerator();
        generator.setMinPrice(1);
        generator.setMaxPrice(3);
        generator.setBasePrice(2.001);
        generator.setMinStepPrice(0.001);
        generator.setStep(1.2);

        List<GridPricePoint> gridPriceList = generator.generateGridPriceList();
        for(GridPricePoint point : gridPriceList){
            System.out.println("price="+point.getPrice()+", diff="+point.getDiff()+", step="+point.getStep());
        }
    }
}
