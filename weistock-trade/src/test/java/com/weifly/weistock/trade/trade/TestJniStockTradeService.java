package com.weifly.weistock.trade.trade;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.trade.TradeTestUtils;
import com.weifly.weistock.trade.config.StockConfigService;
import com.weifly.weistock.trade.config.impl.XmlFileStockConfigService;
import com.weifly.weistock.trade.monitor.StockMonitorService;
import com.weifly.weistock.trade.monitor.impl.StockMonitorServiceImpl;
import com.weifly.weistock.trade.trade.StockOrderInfo;
import com.weifly.weistock.trade.trade.jni.JniStockTradeService;
import org.junit.Test;

import java.util.List;

/**
 * 测试jni
 *
 * @author weifly
 * @since 2019/3/11
 */
public class TestJniStockTradeService {

    @Test
    public void testGetOrderList(){
        StockTradeService stockTradeService = new JniStockTradeService();

        StockMonitorServiceImpl stockMonitorService = new StockMonitorServiceImpl();
        stockMonitorService.setStockTradeService(stockTradeService);
        stockTradeService.setStockMonitorService(stockMonitorService);

        stockMonitorService.updateAccountConfig(TradeTestUtils.createAccountConfig(), false);

        // 第一次获得委托列表
        List<StockOrderInfo> orderList1 = stockTradeService.getOrderList();
        System.out.println("first getOrderList, order size: " + orderList1.size());

        // 更新账号，此时，stockTradeService应该退出登录
        stockMonitorService.updateAccountConfig(TradeTestUtils.createAccountConfig(), false);

        // 第二次获得委托列表
        List<StockOrderInfo> orderList2 = stockTradeService.getOrderList();
        System.out.println("second getOrderList, order size: " + orderList2.size());
    }
}
