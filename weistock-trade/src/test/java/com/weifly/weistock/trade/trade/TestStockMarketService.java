package com.weifly.weistock.trade.trade;

import com.weifly.weistock.trade.trade.tradeX.TradeXMarketSession;
import com.weifly.weistock.trade.trade.tradeX.TradeXSessionFactory;
import org.junit.Test;

public class TestStockMarketService {

    @Test
    public void testGetKLine(){
        TradeXSessionFactory sessionFactory = new TradeXSessionFactory();
        TradeXMarketSession marketSession = sessionFactory.connectMarket("180.153.18.171", 7709);
        try{
            marketSession.getDayKLine("510300", 0, 5);
        }finally {
            marketSession.close();
            sessionFactory.closeLibrary();
        }
    }
}
