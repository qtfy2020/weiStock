package com.weifly.weistock.trade;

import com.weifly.weistock.trade.config.AccountConfigDto;

/**
 * 单元测试相关方法
 *
 * @author weifly
 * @since 2019/8/8
 */
public class TradeTestUtils {

    /**
     * 获得配置文件路径
     */
    public static String getConfigPath(){
        return "d:/TradeInfo.xml";
    }

    /**
     * 创建测试账号
     */
    public static AccountConfigDto createAccountConfig(){
        AccountConfigDto accountConfig = new AccountConfigDto();
        accountConfig.setQsid(2);
        accountConfig.setServerIp("192.168.0.101");
        accountConfig.setServerPort(1234);
        accountConfig.setVersion("1.1");
        accountConfig.setYybID(1);
        accountConfig.setAccountType(8);
        accountConfig.setAccountNo("12345678");
        accountConfig.setTradeAccount("12345678");
        accountConfig.setJyPassword("123456");
        return accountConfig;
    }
}
