package com.weifly.weistock.trade.trade;

import com.weifly.weistock.core.common.Result;
import com.weifly.weistock.trade.trade.SendOrderInfo;
import com.weifly.weistock.trade.trade.StockOrderInfo;
import com.weifly.weistock.trade.trade.StockTradeService;

import java.beans.PropertyChangeEvent;
import java.util.List;

/**
 * 测试用
 *
 * @author weifly
 * @since 2018/11/11
 */
public class StockTradeServiceMock extends StockTradeService {

    public List<StockOrderInfo> getOrderList() {
        return null;
    }

    public Result sendOrder(SendOrderInfo sendOrderInfo) {
        return null;
    }

    public Result cancelOrder(StockOrderInfo stockOrderInfo) {
        return null;
    }

    @Override
    public void logoff() {

    }
}
