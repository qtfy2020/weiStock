package com.weifly.weistock.web;

import com.weifly.weistock.core.util.ModuleBuilder;

import java.util.List;

/**
 * 常量定义
 *
 * @author weifly
 * @since 2019/9/28
 */
public class WeistockConsts {

    public static final String ASSET_CHART_TYPE_DAY = "day"; // 日K线
    public static final String ASSET_CHART_TYPE_WEEK = "week"; // 周K线
    public static final String ASSET_CHART_TYPE_MONTH = "month"; // 月K线
}
