package com.weifly.weistock.web;

import com.weifly.weistock.core.util.ModuleBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 构建各个模块
 *
 * @author weifly
 * @since 2019/9/24
 */
public class WeistockBuilder {

    public static final String ATTR_CONFIG_CDN = "CONFIG_CDN";

    private String configPath; // 根配置路径
    private boolean configCdn; // 开启CDN缓存

    @Autowired
    private List<ModuleBuilder> builderList; // 模块builder列表

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }

    public boolean isConfigCdn() {
        return configCdn;
    }

    public void setConfigCdn(boolean configCdn) {
        this.configCdn = configCdn;
    }

    public void build(){
        if(this.builderList!=null){
            for(ModuleBuilder mb : this.builderList){
                mb.build(this.configPath);
            }
        }
    }
}
