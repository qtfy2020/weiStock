// TC查询，组织机构公用
var TcOrganCondition = function () {};

TcOrganCondition.prototype = {

    organOrgSuggest: null,
    organTcSuggest: null,

    init: function () {
        var self = this;
        var orgList = [];
        orgList.push({id: "3", name: "华东"});
        orgList.push({id: "4", name: "西南"});
        orgList.push({id: "6", name: "华北"});
        orgList.push({id: "10", name: "华南"});
        orgList.push({id: "600", name: "华中"});
        orgList.push({id: "611", name: "东北"});
        orgList.push({id: "645", name: "西北"});

        this.organOrgSuggest = $("#organOrg").magicSuggest({
            editable: false,
            expandOnFocus: true,
            data: orgList,
            mode: "local",
            valueField: "id",
            displayField: "name",
            placeholder: "区域",
            maxSelection: 10,
            maxSelectionRenderer: function () {}
        });
        $(this.organOrgSuggest).on("selectionchange", function(e,m){
            self.organTcSuggest.clear(true);
        });

        this.organTcSuggest = $("#organTc").magicSuggest({
            allowFreeEntries: false,
            mode: "remote",
            data: CONTEXT_PATH + "/init/getTcOrganList",
            valueField: "id",
            displayField: "name",
            placeholder: "场地",
            noSuggestionText: "无场地",
            maxSelection: 10,
            toggleOnClick: true,
            resultsField: "data",
            queryParam: "queryString",
            ajaxConfig: {
                traditional :true
            },
            inputCfg: {
                style: "min-width: 15px;"
            },
            maxSelectionRenderer: function () {}
        });
        $(this.organTcSuggest).on("beforeload", function(me){
            var urlParam = {};
            urlParam.orgId = self.organOrgSuggest.getValue();
            this.setDataUrlParams(urlParam);
        });
    }
};