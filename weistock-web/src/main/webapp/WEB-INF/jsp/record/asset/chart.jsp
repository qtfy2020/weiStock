<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script src="<%=request.getContextPath()%>/static/js/echarts/echarts.js"></script>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .option-table select{
            line-height: 24px;
        }
        .total-table td{
            text-align: center;
        }
        .table-c-l {
            border: none;
            width: 100%;
        }
        .table-c-l td {
            border: none;
            padding: 0px;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var searchBtnObj = null;
        var chartObj = null;

        $(function () {
            chartObj = echarts.init(document.getElementById("chart"));
            initSearchButton();
            searchChart();
        });

        function initSearchButton(){
            searchBtnObj = $("#searchBtn");
            searchBtnObj.on("click", function(){
                searchChart();
            });
        }

        function searchChart(){
            var param = prepareData();
            searchBtnObj.attr("disabled", "disabled");
            $.ajax({
                url: CONTEXT_PATH + "/record/asset/loadChartData",
                cache: false,
                traditional: true,
                data: param,
                type: "get",
                dataType: 'json',
                success: function(result){
                    searchBtnObj.removeAttr("disabled");
                    if(result.code=="200"){
                        renderChart(result.data);
                    }else{
                        alert(result.returnMsg);
                    }
                }
            });
        }

        function prepareData(){
            var param = {};
            param.type = $("#typeField").val();
            param.day = $("#dayField").val();
            var overArray = [];
            $("#overFieldCt").find("input[type='checkbox']:checked").each(function(){
                overArray.push($(this).val());
            });
            param.over = overArray;
            return param;
        }

        function renderChart(data){
            var option = {
                title: { text: "资产走势图" },
                tooltip: {},
                legend: { data: [] },
                xAxis: {
                    data: []
                },
                yAxis: {
                    min: "dataMin"
                },
                series: []
            };

            option.xAxis.data = data.pointList;
            for(var i=0;i<data.serieList.length;i++){
                var serieInfo = data.serieList[i];
                option.legend.data.push(serieInfo.name);
                option.series.push({
                    "name": serieInfo.name,
                    "type": serieInfo.type,
                    "data": serieInfo.dataList,
                    "showAllSymbol": true
                });
            }

            chartObj.setOption(option);
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/record/asset/index">资产记录</a></li>
                <li class="breadcrumb-item active">资产走势图</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <tr>
                    <td style="padding: 0px;">
                        <table class="table-c-l">
                            <tr>
                                <td style="width: 50%">
                                    <table class="table-c-l">
                                        <tr>
                                            <td style="width: 80px;">K线</td>
                                            <td>
                                                <select id="typeField" style="width: 100%;">
                                                    <option value="day" selected>日K线</option>
                                                    <option value="week">周K线</option>
                                                    <option value="month">月K线</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 50%">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 80px;">时间</td>
                                            <td><input id="dayField" type="text" style="width: 100%;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td rowspan="2" style="width: 80px; vertical-align: middle;">
                        <a id="searchBtn" class="btn-sm btn-info">查询</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="table-c-l">
                            <tr>
                                <td style="width: 80px;">叠加</td>
                                <td id="overFieldCt" style="text-align: left;">
                                    <input type="checkbox" value="sh510300" checked>300ETF
                                    <input type="checkbox" value="sh510050">50ETF
                                    <input type="checkbox" value="sh000001">上证指数
                                    <input type="checkbox" value="sz399001">深圳成指
                                    <input type="checkbox" value="sz399006">创业板指数
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div id="chart" style="height:560px;"></div>
        </div>
    </div>
</div>
</body>
</html>
