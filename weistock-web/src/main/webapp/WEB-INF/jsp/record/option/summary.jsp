<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>summary</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";

        $(function () {
            loadSummaryInfo();
        });

        function loadSummaryInfo(){
            var dataParam = {};
            $.ajax({
                url: CONTEXT_PATH + "/record/option/loadSummaryInfo",
                cache: false,
                data: dataParam,
                type: "get",
                dataType: 'json',
                success: function(result){
                    if(result.code=="200"){
                        renderSummaryInfo(result.data);
                    }else{
                        alert(result.message);
                    }
                }
            });
        }

        function renderSummaryInfo(dataMap){
            $("#feeService").html(dataMap.feeService);
            $("#diffAmount").html(dataMap.diffAmount);
            $("#diffAmountUp").html(dataMap.diffAmountUp);
            $("#diffAmountDown").html(dataMap.diffAmountDown);
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/option/index">期权模块</a></li>
                <li class="breadcrumb-item active">期权统计</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <tr>
                    <td class="tit" style="width: 180px;">手续费</td>
                    <td id="feeService"></td>
                </tr>
                <tr>
                    <td class="tit">配对结算额</td>
                    <td id="diffAmount"></td>
                </tr>
                <tr>
                    <td class="tit">配对结算正值</td>
                    <td id="diffAmountUp"></td>
                </tr>
                <tr>
                    <td class="tit">配对结算负值</td>
                    <td id="diffAmountDown"></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>