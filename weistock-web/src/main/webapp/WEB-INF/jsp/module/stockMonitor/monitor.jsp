<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>股票监控</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";

        $(function () {
            refreshStockStatus();
        });

        function refreshStockStatus() {
            $.ajax({
                url: CONTEXT_PATH + "/module/stockMonitor/loadMonitorStatus",
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    if(result.code=="200"){
                        renderStockList(result.data);
                        setTimeout(refreshStockStatus, 5000);
                    }else{
                        alert(result.message);
                    }
                }
            });
        }

        function renderStockList(monitorInfo){
            $("#statusField").html(makeStatusString(monitorInfo.status));
            $("#dateField").html(monitorInfo.date);
            $("#timeField").html(monitorInfo.time);

            var stockArray = [];
            if(monitorInfo.stockList){
                for(var i=0;i<monitorInfo.stockList.length;i++){
                    var oneInfo = monitorInfo.stockList[i];
                    var updateUrl = CONTEXT_PATH + "/module/stockMonitor/updateStockMonitor?stockCode=" + oneInfo.stockCode;
                    var nowPrice = oneInfo.nowPrice!=undefined ? oneInfo.nowPrice : "";
                    var nowRate = oneInfo.nowRate!=undefined ? makeUpOrDownStr(oneInfo.nowRate) : "";
                    var priceDate = oneInfo.priceDate!=undefined ? oneInfo.priceDate : "";
                    var priceTime = oneInfo.priceTime!=undefined ? oneInfo.priceTime : "";
                    var upRate = oneInfo.upRate!=undefined ? oneInfo.upRate : "";
                    var downRate = oneInfo.downRate!=undefined ? oneInfo.downRate : "";
                    var tradeStockName = oneInfo.tradeStockName!=undefined ? oneInfo.tradeStockName : "";
                    var tradeNowPrice = oneInfo.tradeNowPrice!=undefined ? oneInfo.tradeNowPrice : "";
                    var tradeNowRate = oneInfo.tradeNowRate!=undefined ? makeUpOrDownStr(oneInfo.tradeNowRate) : "";
                    var tradeLastPrice = oneInfo.tradeLastPrice!=undefined ? oneInfo.tradeLastPrice : "";
                    var tradeLastRate = oneInfo.tradeLastRate!=undefined ? makeUpOrDownStr(oneInfo.tradeLastRate) : "";
                    var oneStr = "<tr>"+
                        "<td title=\"" + oneInfo.stockCode + "\"><a href=\"" + updateUrl + "\" target=\"_blank\">" + oneInfo.stockName + "</a></td>"+
                        "<td class=\"text-right\">" + nowPrice + "</td>"+
                        "<td class=\"text-right\">" + nowRate + "</td>"+
                        "<td class=\"text-right\">" + priceDate + "&nbsp;" + priceTime + "</td>"+
                        "<td class=\"text-right\">" + upRate + "</td>"+
                        makeSignalHtml(oneInfo.upRateAllPercent)+
                        "<td class=\"text-right\">" + downRate + "</td>"+
                        makeSignalHtml(oneInfo.downRateAllPercent)+
                        makeSignalHtml(oneInfo.diffRatePercent)+
                        "<td class=\"text-right\">" + tradeStockName + "</td>"+
                        "<td class=\"text-right\">" + tradeNowPrice + "</td>"+
                        "<td class=\"text-right\">" + tradeNowRate + "</td>"+
                        "<td class=\"text-right\">" + tradeLastPrice + "</td>"+
                        "<td class=\"text-right\">" + tradeLastRate + "</td>"+
                        "</tr>";
                    stockArray.push(oneStr);
                }
            }
            $("#stockListCt").html(stockArray.join(""));

            var msgArray = [];
            if(monitorInfo.messageList){
                var msgSize = monitorInfo.messageList.length;
                for(var i=(msgSize-1);i>=0;i--){
                    var oneInfo = monitorInfo.messageList[i];
                    var oneStr = "<tr><td>"+oneInfo.time+"</td><td>"+oneInfo.message+"</td></tr>";
                    msgArray.push(oneStr);
                }
            }
            $("#messageListCt").html(msgArray.join(""));
        }

        function makeStatusString(status){
            if(status==1){
                return "初始化";
            }else if(status==2){
                return "休眠&nbsp;非交易日";
            }else if(status==3){
                return "休眠&nbsp;非交易时间";
            }else if(status==4){
                return "判断";
            }else if(status==5){
                return "运行";
            }else{
                return "未知";
            }
        }

        function makeUpOrDownStr(upOrDownRate){
            var rateStr = "";
            var rateColor = "black";  // style="color:red;"
            if(upOrDownRate>0){
                rateStr = "+" + upOrDownRate + "%";
                rateColor = "red";
            }else if(upOrDownRate<0){
                rateStr = upOrDownRate + "%";
                rateColor = "green";
            }
            return "<span style=\"color: " + rateColor + "; font-weight: bold;\">" + rateStr + "</span>";
        }

        function makeSignalHtml(monitorVal){
            var valStr = "";
            var tdStyle = ""; // td样式
            if(monitorVal!==undefined){
                valStr = monitorVal;
                if(valStr>=80){
                    valStr = "<span style=\"color: white;\">"+valStr+"<span>";
                    tdStyle = "style=\"background-color: darkred;\"";
                }else if(valStr<=-80){
                    valStr = "<span style=\"color: white;\">"+valStr+"<span>";
                    tdStyle = "style=\"background-color: darkgreen;\"";
                }
            }
            return "<td class=\"text-right\" " + tdStyle + ">" + valStr + "</td>";
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockMonitor/index">股票监控数据</a></li>
                    <li class="breadcrumb-item active">股票监控</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <table class="table table-bordered option-table total-table">
                    <tr>
                        <td class="tit">日期</td>
                        <td class="tit">时间</td>
                        <td class="tit" colspan="2">状态</td>
                    </tr>
                    <tr>
                        <td id="dateField"></td>
                        <td id="timeField"></td>
                        <td id="statusField" colspan="2"></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="width: 100%; overflow-x: auto;">
                <table class="table table-bordered option-table">
                    <thead>
                        <tr>
                            <td class="tit">股票</td>
                            <td class="tit">现价</td>
                            <td class="tit">涨幅</td>
                            <td class="tit">更新时间</td>
                            <td class="tit">上涨率</td>
                            <td class="tit">百分比</td>
                            <td class="tit">下跌率</td>
                            <td class="tit">百分比</td>
                            <td class="tit">百分差</td>
                            <td class="tit">交易标的</td>
                            <td class="tit">标的价</td>
                            <td class="tit">涨幅</td>
                            <td class="tit">交易价</td>
                            <td class="tit">涨幅</td>
                        </tr>
                    </thead>
                    <tbody id="stockListCt">
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="width: 100%; overflow-x: auto;">
                <table class="table table-bordered option-table">
                    <colgroup>
                        <col style="width: 80px;">
                    </colgroup>
                    <tbody id="messageListCt">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
