<%@ page import="com.weifly.weistock.module.stockmonitor.bo.StockMonitorConfigBO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String stockCode = "";
    String stockName = "";
    StockMonitorConfigBO stockMonitor = (StockMonitorConfigBO)request.getAttribute("stockMonitor");
    if(stockMonitor!=null){
        stockCode = stockMonitor.getStockCode();
        stockName = stockMonitor.getStockName();
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>dayList</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var stockCode = "<%=request.getParameter("stockCode")%>";
        var recordListCtObj;
        var loadBtnObj;
        var lastRecordInfo;

        $(function () {
            recordListCtObj = $("#recordListCt");
            loadBtnObj = $("#loadBtn");
            initLoadButton();
            loadRecordList();
        });

        function initLoadButton(){
            loadBtnObj.on("click", function(){
                loadRecordList();
            });
        }

        function loadRecordList(){
            loadBtnObj.attr("disabled", "disabled");
            // 组装参数
            var dataParam = {};
            dataParam.stockCode = stockCode;
            if(lastRecordInfo){
                dataParam.date = lastRecordInfo.day;
            }
            $.ajax({
                url: CONTEXT_PATH + "/module/stockMonitor/getDayList",
                cache: false,
                data: dataParam,
                type: "get",
                dataType: 'json',
                success: function(result){
                    loadBtnObj.removeAttr("disabled");
                    if(result.code=="200"){
                        renderRecordList(result.data);
                    }else{
                        alert(result.message);
                    }
                }
            });
        }

        function renderRecordList(dataMap){
            if(dataMap.haveMore){
                loadBtnObj.removeAttr("disabled");
            }else{
                loadBtnObj.attr("disabled", "disabled");
            }
            var recordList = dataMap.recordList;
            if(recordList.length>0){
                for(var i=0;i<recordList.length;i++){
                    var recordInfo = recordList[i];
                    var upLowestDay = "";
                    var upLowestPrice = "";
                    var upDiff = "";
                    var upRate = "";
                    var upRateAllPercent = "";
                    var downHighestDay = "";
                    var downHighestPrice = "";
                    var downDiff = "";
                    var downRate = "";
                    var downRateAllPercent = "";
                    var diffRatePercent = "";
                    var diffRateTdStyle = ""; // td样式
                    if(recordInfo.upLowestDay!=undefined){
                        upLowestDay = recordInfo.upLowestDay;
                        upLowestPrice = recordInfo.upLowestPrice;
                        upDiff = recordInfo.upDiff;
                        upRate = recordInfo.upRate;
                        upRateAllPercent = recordInfo.upRateAllPercent;
                        diffRatePercent = recordInfo.diffRatePercent;
                        if(diffRatePercent>80){
                            diffRatePercent = "<span style='color: white;'>"+diffRatePercent+"<span>";
                            diffRateTdStyle = "style='background-color: darkred;'";
                        }else if(diffRatePercent<-80){
                            diffRatePercent = "<span style='color: white;'>"+diffRatePercent+"<span>";
                            diffRateTdStyle = "style='background-color: darkgreen;'";
                        }
                    }
                    if(recordInfo.downHighestDay!=undefined){
                        downHighestDay = recordInfo.downHighestDay;
                        downHighestPrice = recordInfo.downHighestPrice;
                        downDiff = recordInfo.downDiff;
                        downRate = recordInfo.downRate;
                        downRateAllPercent = recordInfo.downRateAllPercent;
                    }
                    var line = "<tr>"+
                        "<td>" + recordInfo.day + "</td>"+
                        "<td>" + recordInfo.close + "</td>"+
                        "<td>" + recordInfo.diff + "</td>"+
                        "<td>" + recordInfo.rate + "</td>"+
                        "<td>" + upLowestDay + "</td>"+
                        "<td>" + upLowestPrice + "</td>"+
                        "<td>" + upDiff + "</td>"+
                        "<td>" + upRate + "</td>"+
                        "<td>" + upRateAllPercent + "</td>"+
                        "<td>" + downHighestDay + "</td>"+
                        "<td>" + downHighestPrice + "</td>"+
                        "<td>" + downDiff + "</td>"+
                        "<td>" + downRate + "</td>"+
                        "<td>" + downRateAllPercent + "</td>"+
                        "<td " + diffRateTdStyle + ">" + diffRatePercent + "</td>"+
                        "</tr>";
                    recordListCtObj.append(line);
                }
                lastRecordInfo = recordList[recordList.length-1];
            }
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockMonitor/index">股票监控数据</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockMonitor/stockList">股票列表</a></li>
                <li class="breadcrumb-item active">天数据列表</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <tr>
                    <td class="tit">代码</td>
                    <td><%=stockCode%></td>
                    <td class="tit">名称</td>
                    <td><%=stockName%></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <thead>
                    <tr>
                        <td class="tit" rowspan="2">日期</td>
                        <td class="tit" rowspan="2">收盘价</td>
                        <td class="tit" rowspan="2">差额</td>
                        <td class="tit" rowspan="2">幅度%</td>
                        <td class="tit" colspan="5">上涨</td>
                        <td class="tit" colspan="5">下跌</td>
                        <td class="tit" rowspan="2">差额百分比</td>
                    </tr>
                    <tr>
                        <td class="tit">最低日</td>
                        <td class="tit">最低价</td>
                        <td class="tit">差额</td>
                        <td class="tit">幅度%</td>
                        <td class="tit">百分比</td>
                        <td class="tit">最高日</td>
                        <td class="tit">最高价</td>
                        <td class="tit">差额</td>
                        <td class="tit">幅度%</td>
                        <td class="tit">百分比</td>
                    </tr>
                </thead>
                <tbody id="recordListCt">
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <button id="loadBtn" class="btn btn-info">加载更多</button>
        </div>
    </div>
</div>
</body>
</html>
