<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String stockCode = (String)request.getAttribute("stockCode");
    if(stockCode==null || stockCode.trim().length()==0){
        stockCode = "";
    }
    String tradeStockCode = (String)request.getAttribute("tradeStockCode");
    if(tradeStockCode==null || tradeStockCode.trim().length()==0){
        tradeStockCode = "";
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>更新股票监控信息</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var stockCode = "<%=stockCode%>";
        var tradeStockCode = "<%=tradeStockCode%>";

        $(function () {
            initConfigValue();
            initUpdateButton();
        });

        function initConfigValue(){
            if(stockCode!=""){
                $("#stockCodeField").val(stockCode);
            }
            if(tradeStockCode!=""){
                $("#tradeStockCodeField").val(tradeStockCode);
            }
        }

        function initUpdateButton(){
            var updateBtnObj = $("#updateBtn");
            updateBtnObj.on("click", function(){
                $("#updateForm").ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    url: CONTEXT_PATH + "/module/stockMonitor/calcStockMonitor",
                    beforeSubmit: function(arr, $form, options){
                        updateBtnObj.attr("disabled", "disabled").html("更新中...");
                    },
                    success: function(result){
                        updateBtnObj.removeAttr("disabled").html("更新");
                        if(result.code=="200"){
                            $("#updateResult").html("更新成功");
                        }else{
                            alert(result.message);
                        }
                    }
                });
            });
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockMonitor/index">股票监控数据</a></li>
                    <li class="breadcrumb-item active">添加或更新股票监控配置</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <form id="updateForm" method="post">
                    <table class="table table-bordered option-table total-table">
                        <tbody>
                            <tr>
                                <td class="tit">股票代码</td>
                                <td class="tit"><input type="text" id="stockCodeField" name="stockCode"></td>
                            </tr>
                            <tr>
                                <td class="tit">交易标的</td>
                                <td class="tit"><input type="text" id="tradeStockCodeField" name="tradeStockCode"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
                <div id="updateResult"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button id="updateBtn" class="btn btn-info">更新</button>
            </div>
        </div>

    </div>
</body>
</html>
