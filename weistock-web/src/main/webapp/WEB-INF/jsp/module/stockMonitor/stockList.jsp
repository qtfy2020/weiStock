<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stockMonitorList</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var recordListCtObj;
        var updateAllBtn;
        var updateAllFlag = false;

        $(function () {
            recordListCtObj = $("#recordListCt");
            updateAllBtn = $("#updateAllBtn");
            updateAllBtn.on("click", updateAllStockMonitor);
            loadRecordList();
        });

        function loadRecordList(){
            $.ajax({
                url: CONTEXT_PATH + "/module/stockMonitor/getStockList",
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    if(result.code=="200"){
                        renderRecordList(result.data);
                    }else{
                        alert(result.message);
                    }
                }
            });
        }

        function renderRecordList(stockMonitorList){
            if(stockMonitorList.length>0){
                for(var i=0;i<stockMonitorList.length;i++){
                    var stockMonitorInfo = stockMonitorList[i];
                    var dayListUrl = CONTEXT_PATH + "/module/stockMonitor/dayList?stockCode=" + stockMonitorInfo.stockCode;
                    var updateUrl = CONTEXT_PATH + "/module/stockMonitor/updateStockMonitor?stockCode=" + stockMonitorInfo.stockCode;
                    var chartUrl = CONTEXT_PATH + "/module/stockMonitor/chart?stockCode=" + stockMonitorInfo.stockCode;
                    var diffRatePercent = "";
                    var diffRateTdStyle = ""; // td样式
                    if(diffRatePercent!==undefined){
                        diffRatePercent = stockMonitorInfo.diffRatePercent;
                        if(diffRatePercent>80){
                            diffRatePercent = "<span style='color: white;'>"+diffRatePercent+"<span>";
                            diffRateTdStyle = "style='background-color: darkred;'";
                        }else if(diffRatePercent<-80){
                            diffRatePercent = "<span style='color: white;'>"+diffRatePercent+"<span>";
                            diffRateTdStyle = "style='background-color: darkgreen;'";
                        }
                    }
                    var line = "<tr>"+
                        "<td>" +
                            "<a href=\"" + dayListUrl + "\" target=\"_self\">" + stockMonitorInfo.stockCode + "</a>&nbsp;&nbsp;"+
                            "<a href=\"" + updateUrl + "\" target=\"_blank\">更新</a>&nbsp;&nbsp;"+
                            "<a href=\"" + chartUrl + "\" target=\"_self\">图表</a>"+
                        "</td>"+
                        "<td>" + stockMonitorInfo.stockName + "</td>"+
                        "<td>" + stockMonitorInfo.endDay + "</td>"+
                        "<td>" + stockMonitorInfo.upRate + "</td>"+
                        "<td>" + stockMonitorInfo.upRateAllPercent + "</td>"+
                        "<td>" + stockMonitorInfo.downRate + "</td>"+
                        "<td>" + stockMonitorInfo.downRateAllPercent + "</td>"+
                        "<td " + diffRateTdStyle + ">" + diffRatePercent + "</td>"+
                        "</tr>";
                    recordListCtObj.append(line);
                }
            }
        }

        function updateAllStockMonitor(){
            if(updateAllFlag){
                return;
            }
            updateAllFlag = true;
            $.ajax({
                url: CONTEXT_PATH + "/module/stockMonitor/doCalcAllStockMonitor",
                beforeSend: function(xhr){
                    updateAllBtn.attr("disabled", "disabled").html("更新中...");
                },
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    updateAllFlag = false;
                    updateAllBtn.removeAttr("disabled").html("更新所有");
                    if(result.code=="200"){
                        alert("批量更新成功！");
                    }else{
                        alert(result.message);
                    }
                }
            });
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockMonitor/index">股票监控数据</a></li>
                <li class="breadcrumb-item active">股票列表</li>
                <li class="breadcrumb-item"><a id="updateAllBtn" class="btn-sm btn-info">更新所有</a></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <thead>
                    <tr>
                        <td class="tit">股票代码</td>
                        <td class="tit">股票名称</td>
                        <td class="tit">最新天</td>
                        <td class="tit">上涨幅度</td>
                        <td class="tit">上涨百分比</td>
                        <td class="tit">下跌幅度</td>
                        <td class="tit">下跌百分比</td>
                        <td class="tit">差额百分比</td>
                    </tr>
                </thead>
                <tbody id="recordListCt">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
