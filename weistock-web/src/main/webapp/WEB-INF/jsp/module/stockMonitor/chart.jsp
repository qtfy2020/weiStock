<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String stockCode = request.getParameter("stockCode");
    if(stockCode==null || stockCode.trim().isEmpty()){
        stockCode = "";
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>涨跌幅走势图</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script src="<%=request.getContextPath()%>/static/js/echarts/echarts.js"></script>
    <style>
        .option-table select{
            line-height: 24px;
        }
        .table-c-l {
            border: none;
            width: 100%;
        }
        .table-c-l td {
            border: none;
            padding: 0px;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var stockCode = "<%=stockCode%>";
        var searchBtnObj = null;
        var stockChartObj = null;
        var percentChartObj = null;
        var diffRatePercentChartObj = null;

        $(function () {
            if(stockCode==""){
                alert("缺少参数: stockCode");
                return;
            }
            stockChartObj = echarts.init(document.getElementById("stockChart"));
            percentChartObj = echarts.init(document.getElementById("percentChart"));
            diffRatePercentChartObj = echarts.init(document.getElementById("diffRatePercentChart"));
            initSearchButton();
            searchChart();
        });

        function initSearchButton(){
            searchBtnObj = $("#searchBtn");
            searchBtnObj.on("click", function(){
                searchChart();
            });
        }

        function searchChart(){
            var param = {};
            param.day = $("#dayField").val();
            param.stockCode = stockCode;
            searchBtnObj.attr("disabled", "disabled");
            $.ajax({
                url: CONTEXT_PATH + "/module/stockMonitor/loadChartData",
                cache: false,
                traditional: true,
                data: param,
                type: "get",
                dataType: 'json',
                success: function(result){
                    searchBtnObj.removeAttr("disabled");
                    if(result.code=="200"){
                        renderStockChart(result.data);
                        renderPercentChart(result.data);
                        renderDiffRatePercentChart(result.data);
                    }else{
                        alert(result.returnMsg);
                    }
                }
            });
        }

        function renderStockChart(data){
            var option = {
                title: { text: "股票走势图" },
                tooltip: {},
                legend: { data: [] },
                xAxis: {
                    data: []
                },
                yAxis: {
                    min: "dataMin"
                },
                series: []
            };

            option.xAxis.data = data.pointList;
            for(var i=0;i<data.serieList.length;i++){
                var serieInfo = data.serieList[i];
                if(i==0 || serieInfo.name=="最小值" || serieInfo.name=="最大值"){
                    option.legend.data.push(serieInfo.name);
                    option.series.push({
                        "name": serieInfo.name,
                        "type": serieInfo.type,
                        "data": serieInfo.dataList,
                        "showAllSymbol": true
                    });
                }
            }
            stockChartObj.setOption(option);
        }

        function renderPercentChart(data) {
            var option = {
                title: { text: "百分比图" },
                tooltip: {},
                legend: { data: [] },
                xAxis: {
                    data: []
                },
                yAxis: {
                    min: "dataMin"
                },
                series: []
            };

            option.xAxis.data = data.pointList;
            for(var i=0;i<data.serieList.length;i++){
                var serieInfo = data.serieList[i];
                if(serieInfo.name=="上涨百分比" || serieInfo.name=="下跌百分比"){
                    option.legend.data.push(serieInfo.name);
                    option.series.push({
                        "name": serieInfo.name,
                        "type": serieInfo.type,
                        "data": serieInfo.dataList,
                        "showAllSymbol": true
                    });
                }
            }
            percentChartObj.setOption(option);
        }

        function renderDiffRatePercentChart(data) {
            var option = {
                title: { text: "百分比差值" },
                tooltip: {},
                legend: { data: [] },
                xAxis: {
                    data: []
                },
                yAxis: {
                    min: "dataMin"
                },
                series: []
            };

            option.xAxis.data = data.pointList;
            for(var i=0;i<data.serieList.length;i++){
                var serieInfo = data.serieList[i];
                if(serieInfo.name=="百分比差值"){
                    option.legend.data.push(serieInfo.name);
                    option.series.push({
                        "name": serieInfo.name,
                        "type": serieInfo.type,
                        "data": serieInfo.dataList,
                        "showAllSymbol": true
                    });
                }
            }
            diffRatePercentChartObj.setOption(option);
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockMonitor/index">股票监控数据</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockMonitor/stockList">股票列表</a></li>
                <li class="breadcrumb-item active">涨跌幅走势图</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <tr>
                    <td style="padding: 0px;">
                        <table class="table-c-l">
                            <tr>
                                <td style="width: 50%">
                                    <table class="table-c-l">
                                        <tr>
                                            <td style="width: 80px;">K线</td>
                                            <td>
                                                <select id="typeField" style="width: 100%;">
                                                    <option value="day" selected>日K线</option>
                                                    <option value="week">周K线</option>
                                                    <option value="month">月K线</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 50%">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 80px;">时间</td>
                                            <td><input id="dayField" type="text" style="width: 100%;" value="<%=request.getAttribute("lowDay")%>"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td rowspan="2" style="width: 80px; vertical-align: middle;">
                        <a id="searchBtn" class="btn-sm btn-info">查询</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div id="stockChart" style="height:380px;"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div id="percentChart" style="height:380px;"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div id="diffRatePercentChart" style="height:380px;"></div>
        </div>
    </div>
</div>
</body>
</html>
