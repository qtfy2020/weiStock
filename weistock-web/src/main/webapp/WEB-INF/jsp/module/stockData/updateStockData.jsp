<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String stockCode = request.getParameter("stockCode");
    if(stockCode==null || stockCode.trim().length()==0){
        stockCode = "";
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>updateStockData</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var stockCode = "<%=stockCode%>";

        $(function () {
            initAssetDay();
            initUpdateButton();
        });

        function initAssetDay(){
            if(stockCode!=""){
                $("#stockCodeField").val(stockCode);
            }
        }

        function initUpdateButton(){
            var updateBtnObj = $("#updateBtn");
            updateBtnObj.on("click", function(){
                $("#updateForm").ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    url: CONTEXT_PATH + "/module/stockData/doUpdateStockData",
                    beforeSubmit: function(arr, $form, options){
                        updateBtnObj.attr("disabled", "disabled").html("更新中...");
                    },
                    success: function(result){
                        updateBtnObj.removeAttr("disabled").html("更新");
                        if(result.code=="200"){
                            var mergeInfo = result.data;
                            var str = "总记录：" + mergeInfo.total + "，插入：" + mergeInfo.insert + "，更新：" + mergeInfo.update;
                            $("#updateResult").html(str);
                        }else{
                            alert(result.message);
                        }
                    }
                });
            });
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockData/index">股票历史数据</a></li>
                    <li class="breadcrumb-item active">更新股票数据</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <form id="updateForm" method="post">
                    <table class="table table-bordered option-table total-table">
                        <tbody>
                            <tr>
                                <td class="tit">股票代码</td>
                                <td class="tit"><input type="text" id="stockCodeField" name="stockCode"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
                <div id="updateResult"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button id="updateBtn" class="btn btn-info">更新</button>
            </div>
        </div>

    </div>
</body>
</html>
