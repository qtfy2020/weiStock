<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stockDataList</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var recordListCtObj;
        var updateAllBtn;
        var updateAllFlag = false;

        $(function () {
            recordListCtObj = $("#recordListCt");
            updateAllBtn = $("#updateAllBtn");
            updateAllBtn.on("click", updateAllStockData);
            loadRecordList();
        });

        function loadRecordList(){
            $.ajax({
                url: CONTEXT_PATH + "/module/stockData/getStockList",
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    if(result.code=="200"){
                        renderRecordList(result.data);
                    }else{
                        alert(result.message);
                    }
                }
            });
        }

        function renderRecordList(stockDataList){
            if(stockDataList.length>0){
                for(var i=0;i<stockDataList.length;i++){
                    var stockDataInfo = stockDataList[i];
                    var dayListUrl = CONTEXT_PATH + "/module/stockData/dayList?stockCode=" + stockDataInfo.stockCode;
                    var updateUrl = CONTEXT_PATH + "/module/stockData/updateStockData?stockCode=" + stockDataInfo.stockCode;
                    var line = "<tr>"+
                            "<td><a href=\"" + dayListUrl + "\" target=\"_self\">" + stockDataInfo.stockCode + "</a>&nbsp;&nbsp;<a href=\"" + updateUrl + "\" target=\"_self\">更新</a></td>"+
                            "<td>" + stockDataInfo.stockName + "</td>"+
                            "<td>" + stockDataInfo.startDay + "</td>"+
                            "<td>" + stockDataInfo.startValue + "</td>"+
                            "<td>" + stockDataInfo.endDay + "</td>"+
                            "<td>" + stockDataInfo.endValue + "</td>"+
                            "</tr>";
                    recordListCtObj.append(line);
                }
            }
        }

        function updateAllStockData(){
            if(updateAllFlag){
                return;
            }
            updateAllFlag = true;
            $.ajax({
                url: CONTEXT_PATH + "/module/stockData/doUpdateAllStockData",
                beforeSend: function(xhr){
                    updateAllBtn.attr("disabled", "disabled").html("更新中...");
                },
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    updateAllFlag = false;
                    updateAllBtn.removeAttr("disabled").html("更新所有");
                    if(result.code=="200"){
                        alert("批量更新成功！");
                    }else{
                        alert(result.message);
                    }
                }
            });
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockData/index">股票历史数据</a></li>
                <li class="breadcrumb-item active">股票列表</li>
                <li class="breadcrumb-item"><a id="updateAllBtn" class="btn-sm btn-info">更新所有</a></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <thead>
                    <tr>
                        <td class="tit">股票代码</td>
                        <td class="tit">股票名称</td>
                        <td class="tit">开始日期</td>
                        <td class="tit">开始值</td>
                        <td class="tit">结束日期</td>
                        <td class="tit">结束值</td>
                    </tr>
                </thead>
                <tbody id="recordListCt">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
