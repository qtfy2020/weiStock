<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>dayList</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var stockCode = "<%=request.getParameter("stockCode")%>";
        var recordListCtObj;
        var loadBtnObj;
        var lastRecordInfo;

        $(function () {
            recordListCtObj = $("#recordListCt");
            loadBtnObj = $("#loadBtn");
            initLoadButton();
            loadRecordList();
        });

        function initLoadButton(){
            loadBtnObj.on("click", function(){
                loadRecordList();
            });
        }

        function loadRecordList(){
            loadBtnObj.attr("disabled", "disabled");
            // 组装参数
            var dataParam = {};
            dataParam.stockCode = stockCode;
            if(lastRecordInfo){
                dataParam.date = lastRecordInfo.day;
            }
            $.ajax({
                url: CONTEXT_PATH + "/module/stockData/getDayList",
                cache: false,
                data: dataParam,
                type: "get",
                dataType: 'json',
                success: function(result){
                    loadBtnObj.removeAttr("disabled");
                    if(result.code=="200"){
                        renderRecordList(result.data);
                    }else{
                        alert(result.message);
                    }
                }
            });
        }

        function renderRecordList(dataMap){
            if(dataMap.haveMore){
                loadBtnObj.removeAttr("disabled");
            }else{
                loadBtnObj.attr("disabled", "disabled");
            }
            var recordList = dataMap.recordList;
            if(recordList.length>0){
                for(var i=0;i<recordList.length;i++){
                    var recordInfo = recordList[i];
                    var line = "<tr>"+
                        "<td>" + recordInfo.day + "</td>"+
                        "<td>" + recordInfo.close + "</td>"+
                        "</tr>";
                    recordListCtObj.append(line);
                }
                lastRecordInfo = recordList[recordList.length-1];
            }
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockData/index">股票历史数据</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockData/stockList">股票列表</a></li>
                <li class="breadcrumb-item active">天数据列表</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <thead>
                    <tr>
                        <td class="tit">日期</td>
                        <td class="tit">收盘价</td>
                    </tr>
                </thead>
                <tbody id="recordListCt">
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <button id="loadBtn" class="btn btn-info">加载更多</button>
        </div>
    </div>
</div>
</body>
</html>
