<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var stockCode = "<%=request.getParameter("stockCode")%>";
        var stockHoldSummary = <%=request.getAttribute("stockHoldSummary")%>;

        $(function () {
            renderStockHoldInfo();
        });

        function renderStockHoldInfo(){
            $("#stockCodeField").html(stockHoldSummary.stockCode);
            $("#stockNameField").html(stockHoldSummary.stockName);
            $("#holdNumberField").html(stockHoldSummary.holdNumber);
            var dayList = stockHoldSummary.dayList;
            if(dayList.length>0){
                var dayListCtObj = $("#dayListCt");
                for(var i=0;i<dayList.length;i++){
                    var dayInfo = dayList[i];
                    var line = "<tr>"+
                        "<td>" + dayInfo.date + "</td>"+
                        "<td>" + dayInfo.holdNumber + "</td>"+
                        "<td>" + dayInfo.totalNumber + "</td>"+
                        "<td>" + dayInfo.holdDays + "</td>"+
                        "</tr>";
                    dayListCtObj.append(line);
                }
            }
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockBill/index">股票交易记录</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockBill/stockList">股票列表</a></li>
                <li class="breadcrumb-item active">净持仓统计</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <tr>
                    <td class="tit">股票代码</td>
                    <td class="tit">股票名称</td>
                    <td class="tit">持仓数量</td>
                </tr>
                <tr>
                    <td id="stockCodeField"></td>
                    <td id="stockNameField"></td>
                    <td id="holdNumberField"></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <thead>
                    <tr>
                        <td class="tit">购买日期</td>
                        <td class="tit">持仓数量</td>
                        <td class="tit">总持仓量</td>
                        <td class="tit">持仓天数</td>
                    </tr>
                </thead>
                <tbody id="dayListCt">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
