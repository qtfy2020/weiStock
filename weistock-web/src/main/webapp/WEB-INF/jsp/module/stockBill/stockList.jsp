<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var recordListCtObj;

        $(function () {
            recordListCtObj = $("#recordListCt");
            loadRecordList();
        });

        function loadRecordList(){
            $.ajax({
                url: CONTEXT_PATH + "/module/stockBill/getStockList",
                cache: false,
                data: {},
                type: "get",
                dataType: 'json',
                success: function(result){
                    if(result.code=="200"){
                        renderRecordList(result.data);
                    }else{
                        alert(result.message);
                    }
                }
            });
        }

        function renderRecordList(stockSummaryList){
            if(stockSummaryList.length>0){
                for(var i=0;i<stockSummaryList.length;i++){
                    var stockSummaryInfo = stockSummaryList[i];
                    var recordUrl = CONTEXT_PATH + "/module/stockBill/recordList?stockCode=" + stockSummaryInfo.stockCode;

                    var line = "<tr>"+
                            "<td><a href=\""+recordUrl+"\" target=\"_self\">" + stockSummaryInfo.stockCode + "</a></td>"+
                            "<td>" + stockSummaryInfo.stockName + "</td>"+
                            "<td>" + stockSummaryInfo.recordSize + "</td>"+
                            "<td>" + (stockSummaryInfo.diffAmount==null ? "" : stockSummaryInfo.diffAmount) + "</td>"+
                            "<td>" + (stockSummaryInfo.feeService==null ? "" : stockSummaryInfo.feeService) + "</td>"+
                            "<td>" + (stockSummaryInfo.feeStamp==null ? "" : stockSummaryInfo.feeStamp) + "</td>"+
                            "<td>" + (stockSummaryInfo.afterNumber==null ? "" : stockSummaryInfo.afterNumber) + "</td>"+
                            "<td>" + stockSummaryInfo.lastOrderTime + "</td>"+
                            "</tr>";
                    recordListCtObj.append(line);
                }
            }
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockBill/index">股票交易记录</a></li>
                <li class="breadcrumb-item active">股票列表</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <thead>
                    <tr>
                        <td class="tit">股票代码</td>
                        <td class="tit">股票名称</td>
                        <td class="tit">交易次数</td>
                        <td class="tit">差额</td>
                        <td class="tit">手续费</td>
                        <td class="tit">印花税</td>
                        <td class="tit">剩余数量</td>
                        <td class="tit">最后交易时间</td>
                    </tr>
                </thead>
                <tbody id="recordListCt">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
