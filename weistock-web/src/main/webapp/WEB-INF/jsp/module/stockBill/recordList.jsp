<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Map" %>
<%
    Map summaryMap = (Map)request.getAttribute("stockSummary");
    String stockCode = request.getParameter("stockCode");
%>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var stockCode = "<%=request.getParameter("stockCode")%>";
        var recordListCtObj;
        var loadBtnObj;
        var lastRecordInfo;

        $(function () {
            recordListCtObj = $("#recordListCt");
            loadBtnObj = $("#loadBtn");
            initLoadButton();
            loadRecordList();
        });

        function initLoadButton(){
            loadBtnObj.on("click", function(){
                loadRecordList();
            });
        }

        function loadRecordList(){
            loadBtnObj.attr("disabled", "disabled");
            // 组装参数
            var dataParam = {};
            dataParam.stockCode = stockCode;
            if(lastRecordInfo){
                dataParam.date = lastRecordInfo.date;
                dataParam.time = lastRecordInfo.time;
                dataParam.entrust = lastRecordInfo.entrustCode;
            }

            $.ajax({
                url: CONTEXT_PATH + "/module/stockBill/getRecordList",
                cache: false,
                data: dataParam,
                type: "get",
                dataType: 'json',
                success: function(result){
                    loadBtnObj.removeAttr("disabled");
                    if(result.code=="200"){
                        renderRecordList(result.data);
                    }else{
                        alert(result.message);
                    }
                }
            });
        }

        function renderRecordList(dataMap){
            if(dataMap.haveMore){
                loadBtnObj.removeAttr("disabled");
            }else{
                loadBtnObj.attr("disabled", "disabled");
            }

            var recordList = dataMap.recordList;
            if(recordList.length>0){
                for(var i=0;i<recordList.length;i++){
                    var recordInfo = recordList[i];
                    var line = "<tr>"+
                        "<td>" + recordInfo.date + "</td>"+
                        "<td>" + recordInfo.time + "</td>"+
                        "<td>" + recordInfo.businessName + "</td>"+
                        "<td>" + recordInfo.tradePrice + "</td>"+
                        "<td>" + recordInfo.tradeNumber + "</td>"+
                        "<td>" + recordInfo.tradeAmount + "</td>"+
                        "<td>" + recordInfo.feeService + "</td>"+
                        "<td>" + recordInfo.feeStamp + "</td>"+
                        "<td>" + recordInfo.clearAmount + "</td>"+
                        "<td>" + recordInfo.entrustCode + "</td>"+
                        "<td>" + (recordInfo.diffAmount ? recordInfo.diffAmount : "") + "</td>"+
                        "<td>" + (recordInfo.pair ? recordInfo.pair : "") + "</td>"+
                        "</tr>";
                    recordListCtObj.append(line);
                }
                lastRecordInfo = recordList[recordList.length-1];
            }
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockBill/index">股票交易记录</a></li>
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/module/stockBill/stockList">股票列表</a></li>
                <li class="breadcrumb-item active">交易记录列表</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <tr>
                    <td class="tit">股票代码</td>
                    <td class="tit">股票名称</td>
                    <td class="tit">交易次数</td>
                    <td class="tit">差额</td>
                    <td class="tit">手续费</td>
                    <td class="tit">印花税</td>
                    <td class="tit">剩余数量</td>
                </tr>
                <tr>
                    <td id="stockCodeField"><%=summaryMap.get("stockCode")%></td>
                    <td id="stockNameField"><%=summaryMap.get("stockName")%></td>
                    <td id="recordSizeField"><%=summaryMap.get("recordSize")%></td>
                    <td id="diffAmountField"><%=summaryMap.get("diffAmount")%></td>
                    <td id="feeServiceField"><%=summaryMap.get("feeService")%></td>
                    <td id="feeStampField"><%=summaryMap.get("feeStamp")%></td>
                    <td id="afterNumberField">
                        <a href="<%=request.getContextPath()%>/module/stockBill/stockHoldSummary?stockCode=<%=stockCode%>"><%=summaryMap.get("afterNumber")%></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12" style="overflow-x: auto;">
            <table class="table table-bordered option-table total-table">
                <thead>
                    <tr>
                        <td class="tit">日期</td>
                        <td class="tit">时间</td>
                        <td class="tit">业务名称</td>
                        <td class="tit">成交价格</td>
                        <td class="tit">成交数量</td>
                        <td class="tit">成交金额</td>
                        <td class="tit">手续费</td>
                        <td class="tit">印花税</td>
                        <td class="tit">发生金额</td>
                        <td class="tit">委托编号</td>
                        <td class="tit">差额</td>
                        <td class="tit">配对</td>
                    </tr>
                </thead>
                <tbody id="recordListCt">
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <button id="loadBtn" class="btn btn-info">加载更多</button>
        </div>
    </div>
</div>
</body>
</html>
