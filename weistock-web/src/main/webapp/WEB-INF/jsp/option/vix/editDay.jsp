<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>编辑VIX记录</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
        .total-table td input{
            width: 100%;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        $(function () {
            initUpdateButton();
        });

        function initUpdateButton(){
            var updateBtnObj = $("#updateBtn");
            updateBtnObj.on("click", function(){
                $("#updateForm").ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    url: CONTEXT_PATH + "/option/vix/updateDay",
                    beforeSubmit: function(arr, $form, options){
                        updateBtnObj.attr("disabled", "disabled").html("保存中...");
                    },
                    success: function(result){
                        updateBtnObj.removeAttr("disabled").html("保存");
                        if(result.code=="200"){
                            alert("保存成功");
                        }else{
                            alert(result.message);
                        }
                    }
                });
            });
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/option/index">期权模块</a></li>
                    <li class="breadcrumb-item active">添加VIX记录</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <form id="updateForm" method="post">
                    <table class="table table-bordered option-table total-table">
                        <tbody>
                            <tr>
                                <td class="tit">日期</td>
                                <td class="tit"><input type="text" id="dayField" name="dayField"></td>
                            </tr>
                            <tr>
                                <td class="tit">开盘价</td>
                                <td class="tit"><input type="text" id="openField" name="openField"></td>
                            </tr>
                            <tr>
                                <td class="tit">最高价</td>
                                <td class="tit"><input type="text" id="highField" name="highField"></td>
                            </tr>
                            <tr>
                                <td class="tit">最低价</td>
                                <td class="tit"><input type="text" id="lowField" name="lowField"></td>
                            </tr>
                            <tr>
                                <td class="tit">收盘价</td>
                                <td class="tit"><input type="text" id="closeField" name="closeField"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button id="updateBtn" class="btn btn-info">保存</button>
            </div>
        </div>

    </div>
</body>
</html>
