<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>期权</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item active">期权</li>
                </ul>
            </div>
        </div>

        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/record/option/import">导入交割单</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/record/option/summary">期权统计</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/record/option/list">期权记录列表</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/option/vix/editDay">VIX-添加记录</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/option/vix/list">VIX-记录列表</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/option/vix/summary">VIX-统计</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/option/vix/chart">VIX-走势图</a>
            </div>
        </div>
    </div>
</body>
</html>
