<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String stockConfig = (String)request.getAttribute("stockConfig");
    if(stockConfig==null){
        stockConfig = "{}";
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>网格配置</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .text-left{
            text-align: left;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
        .total-table td input{
            width: 100%;
        }
        .total-table td select{
            width: 100%;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var stockConfig = <%=stockConfig%>;

        var loadStockBtnObj;
        var updateStockBtnObj;
        var lastStockCode;
        $(function () {
            renderInitConfig();
            initLoadStockButton();
            initUpdateStockButton();
        });

        function renderInitConfig() {
            if(stockConfig.stockCode!==undefined){
                lastStockCode = stockConfig.stockCode;
                $("#stockCode").val(stockConfig.stockCode);
            }
            if(stockConfig.open!==undefined){
                if(stockConfig.open){
                    $("#open").attr("checked", "checked");
                }else {
                    $("#open").removeAttr("checked");
                }
            }
            if(stockConfig.tradeUnit!==undefined){
                $("#tradeUnit").val(stockConfig.tradeUnit);
            }
            if(stockConfig.step!==undefined){
                $("#step").val(stockConfig.step);
            }
            if(stockConfig.lastOperation!==undefined){
                $("#lastOperation_" + stockConfig.lastOperation).attr("selected", "selected");
            }
            if(stockConfig.gridGapNumber!==undefined){
                $("#gridGapNumber").val(stockConfig.gridGapNumber);
            }
            if(stockConfig.creditBuy!==undefined){
                if(stockConfig.creditBuy){
                    $("#creditBuy").attr("checked", "checked");
                }else {
                    $("#creditBuy").removeAttr("checked");
                }
            }
            renderStockConfigInfo(stockConfig);
        }

        function initLoadStockButton(){
            loadStockBtnObj = $("#loadStockBtn");
            loadStockBtnObj.on("click", function(){
                var stockCode = $("#stockCode").val();
                if(stockCode==null || stockCode==""){
                    alert("请填写股票代码");
                    return;
                }
                var step = $("#step").val();
                if(step==null || step==""){
                    alert("请填写网格间距");
                    return;
                }
                var basePrice = $("#basePrice").val();
                if(stockCode!=lastStockCode){
                    basePrice = ""; // 股票代码不同时，清空basePrice
                }

                $.ajax({
                    url: CONTEXT_PATH + "/stock/makeStockConfig",
                    cache: false,
                    data: {
                        "stockCode": stockCode,
                        "step": step,
                        "basePrice": basePrice
                    },
                    type: "get",
                    dataType: 'json',
                    beforeSend: function(){
                        disablePageButton();
                    },
                    success: function(result){
                        enablePageButton();
                        if(result.code=="200"){
                            lastStockCode = stockCode;
                            renderStockConfigInfo(result.data);
                        }else{
                            alert(result.message);
                        }
                    }
                });
            });
        }

        function renderStockConfigInfo(stockConfig){
            if(stockConfig.stockName!==undefined){
                $("#stockName").val(stockConfig.stockName);
            }
            if(stockConfig.exchangeId!==undefined){
                $("#exchangeId").val(stockConfig.exchangeId);
            }
            if(stockConfig.basePrice!==undefined){
                $("#basePrice").val(stockConfig.basePrice);
            }

            var priceArray = [];
            if(stockConfig.gridPriceList){
                for(var i=0;i<stockConfig.gridPriceList.length;i++){
                    var oneInfo = stockConfig.gridPriceList[i];
                    var oneStr = "<tr><td class=\"text-left gridPriceItem\">" + oneInfo.price + "</td>"
                        +"<td class=\"text-left\">" + oneInfo.diff + "</td>"
                        +"<td class=\"text-left\">" + oneInfo.step + "</td>"
                        +"</tr>";
                    priceArray.push(oneStr);
                }
            }
            $("#gridPriceList").html(priceArray.join(""));
        }

        function initUpdateStockButton(){
            updateStockBtnObj = $("#updateStockBtn");
            updateStockBtnObj.on("click", function(){
                var priceArray = [];
                var priceItems = $("#gridPriceList").find(".gridPriceItem").each(function(){
                    priceArray.push($(this).text());
                });
                var priceStr = priceArray.join(",");
                $("#gridPrice").val(priceStr);

                $("#uploadStockForm").ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    url: CONTEXT_PATH + "/stock/updateStockConfig",
                    beforeSubmit: function(arr, $form, options){
                        disablePageButton();
                        updateStockBtnObj.html("保存中...");
                    },
                    success: function(result){
                        enablePageButton();
                        updateStockBtnObj.html("保存");
                        if(result.code=="200"){
                            alert("保存成功");
                        }else{
                            alert(result.message);
                        }
                    }
                });
            });
        }

        function disablePageButton(){
            loadStockBtnObj.attr("disabled", "disabled");
            updateStockBtnObj.attr("disabled", "disabled");
        }

        function enablePageButton(){
            loadStockBtnObj.removeAttr("disabled")
            updateStockBtnObj.removeAttr("disabled")
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/stock/index">网格交易</a></li>
                    <li class="breadcrumb-item active">网格配置</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <form id="uploadStockForm" method="post">
                    <table class="table table-bordered option-table total-table">
                        <tbody>
                            <tr>
                                <td class="tit">股票代码</td>
                                <td class="tit"><input type="text" id="stockCode" name="stockCode"></td>
                                <td><button id="loadStockBtn" type="button" class="btn-sm btn-info">加载股票</button></td>
                            </tr>
                            <tr>
                                <td class="tit">股票名称</td>
                                <td colspan="2" class="tit"><input type="text" id="stockName" name="stockName" readonly></td>
                            </tr>
                            <tr>
                                <td class="tit">交易所ID</td>
                                <td colspan="2" class="tit"><input type="text" id="exchangeId" name="exchangeId" readonly></td>
                            </tr>
                            <tr>
                                <td class="tit">是否开启</td>
                                <td colspan="2" class="text-left"><input type="checkbox" id="open" name="open" value="true" checked style="width: auto;"></td>
                            </tr>
                            <tr>
                                <td class="tit">基准价格</td>
                                <td colspan="2" class="tit"><input type="text" id="basePrice" name="basePrice"></td>
                            </tr>
                            <tr>
                                <td class="tit">每次交易量</td>
                                <td colspan="2" class="tit"><input type="text" id="tradeUnit" name="tradeUnit" value="1000"></td>
                            </tr>
                            <tr>
                                <td class="tit">网格间距</td>
                                <td class="tit"><input type="text" id="step" name="step" value="1"></td>
                                <td>百分比(1-10)</td>
                            </tr>
                            <tr>
                                <td class="tit">最后一次操作类型</td>
                                <td class="tit">
                                    <select id="lastOperation" name="lastOperation">
                                        <option id="eem"></option>
                                        <option id="lastOperation_buy" value="buy">买</option>
                                        <option id="lastOperation_sell" value="sell">卖</option>
                                    </select>
                                </td>
                                <td>非对称网格使用</td>
                            </tr>
                            <tr>
                                <td class="tit">网格间隔数</td>
                                <td class="tit"><input type="text" id="gridGapNumber" name="gridGapNumber" value=""></td>
                                <td>非对称网格使用</td>
                            </tr>
                            <tr>
                                <td class="tit">融资买入</td>
                                <td colspan="2" class="text-left"><input type="checkbox" id="creditBuy" name="creditBuy" value="true" style="width: auto;"></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-bordered option-table total-table">
                        <thead>
                            <tr>
                                <td class="tit">价格</td>
                                <td class="tit">价差</td>
                                <td class="tit">幅度</td>
                            </tr>
                        </thead>
                        <tbody id="gridPriceList">
                        </tbody>
                    </table>
                    <input id="gridPrice" type="hidden" name="gridPrice">
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button id="updateStockBtn" class="btn btn-info">保存</button>
            </div>
        </div>

    </div>
</body>
</html>
