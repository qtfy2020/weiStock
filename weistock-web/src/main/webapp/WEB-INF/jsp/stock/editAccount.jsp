<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String accountConfig = (String)request.getAttribute("accountConfig");
    if(accountConfig==null){
        accountConfig = "{}";
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>编辑股票账号</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
        .total-table td input{
            width: 100%;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var accountConfig = <%=accountConfig%>;

        $(function () {
            initAccountConfig();
            initUpdateAccountButton();
        });

        function initAccountConfig(){
            if(accountConfig.qsid){
                $("#qsid").val(accountConfig.qsid);
            }
            if(accountConfig.serverIp){
                $("#serverIp").val(accountConfig.serverIp);
            }
            if(accountConfig.serverPort){
                $("#serverPort").val(accountConfig.serverPort);
            }
            if(accountConfig.version){
                $("#version").val(accountConfig.version);
            }
            if(accountConfig.yybID){
                $("#yybID").val(accountConfig.yybID);
            }
            if(accountConfig.accountType){
                $("#accountType").val(accountConfig.accountType);
            }
            if(accountConfig.accountNo){
                $("#accountNo").val(accountConfig.accountNo);
            }
            if(accountConfig.tradeAccount){
                $("#tradeAccount").val(accountConfig.tradeAccount);
            }
            if(accountConfig.jyPassword){
                $("#jyPassword").val(accountConfig.jyPassword);
            }
            if(accountConfig.txPassword){
                $("#txPassword").val(accountConfig.txPassword);
            }
        }

        function initUpdateAccountButton(){
            var updateAccountBtnObj = $("#updateAccountBtn");
            updateAccountBtnObj.on("click", function(){
                $("#uploadAccountForm").ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    url: CONTEXT_PATH + "/stock/updateAccount",
                    beforeSubmit: function(arr, $form, options){
                        updateAccountBtnObj.attr("disabled", "disabled").html("保存中...");
                    },
                    success: function(result){
                        updateAccountBtnObj.removeAttr("disabled").html("保存");
                        if(result.code=="200"){
                            alert("保存成功");
                        }else{
                            alert(result.message);
                        }
                    }
                });
            });
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/stock/index">网格交易</a></li>
                    <li class="breadcrumb-item active">配置账号</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <form id="uploadAccountForm" method="post">
                    <table class="table table-bordered option-table total-table">
                        <tbody>
                            <tr>
                                <td class="tit">券商id</td>
                                <td class="tit"><input type="text" id="qsid" name="qsid"></td>
                            </tr>
                            <tr>
                                <td class="tit">交易服务器ip</td>
                                <td class="tit"><input type="text" id="serverIp" name="serverIp"></td>
                            </tr>
                            <tr>
                                <td class="tit">交易服务器端口</td>
                                <td class="tit"><input type="text" id="serverPort" name="serverPort"></td>
                            </tr>
                            <tr>
                                <td class="tit">版本</td>
                                <td class="tit"><input type="text" id="version" name="version"></td>
                            </tr>
                            <tr>
                                <td class="tit">营业部id</td>
                                <td class="tit"><input type="text" id="yybID" name="yybID"></td>
                            </tr>
                            <tr>
                                <td class="tit">账户类型</td>
                                <td class="tit"><input type="text" id="accountType" name="accountType"></td>
                            </tr>
                            <tr>
                                <td class="tit">客户账号</td>
                                <td class="tit"><input type="text" id="accountNo" name="accountNo"></td>
                            </tr>
                            <tr>
                                <td class="tit">交易账号</td>
                                <td class="tit"><input type="text" id="tradeAccount" name="tradeAccount"></td>
                            </tr>
                            <tr>
                                <td class="tit">交易密码</td>
                                <td class="tit"><input type="password" id="jyPassword" name="jyPassword"></td>
                            </tr>
                            <tr>
                                <td class="tit">通信密码</td>
                                <td class="tit"><input type="text" id="txPassword" name="txPassword"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button id="updateAccountBtn" class="btn btn-info">保存</button>
            </div>
        </div>

    </div>
</body>
</html>
