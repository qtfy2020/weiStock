<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                <li class="breadcrumb-item active">网格交易</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-bottom: 4px;">
        <div class="col-sm-12" style="text-align: center;">
            <a class="btn btn-info" href="<%=request.getContextPath()%>/stock/monitor">股票监控</a>
        </div>
    </div>
    <div class="row" style="margin-bottom: 4px;">
        <div class="col-sm-12" style="text-align: center;">
            <a class="btn btn-info" href="<%=request.getContextPath()%>/stock/editStockConfig">添加网格</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12" style="text-align: center;">
            <a class="btn btn-info" href="<%=request.getContextPath()%>/stock/editAccount">配置账户</a>
        </div>
    </div>
</div>
</body>
</html>
