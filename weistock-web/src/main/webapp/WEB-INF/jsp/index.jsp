<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>stock-web</title>
    <%@include file="layout/head.jsp"%>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item active">首页</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="text-align: center;">
                <h1>欢迎使用</h1>
            </div>
        </div>

        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/stock/index">网格交易</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/record/asset/index">资产记录</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/option/index">期权模块</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/module/stockBill/index">股票交易记录</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/module/stockData/index">股票历史数据</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/module/stockMonitor/index">股票监控数据</a>
            </div>
        </div>
        <div class="row" style="margin-bottom: 4px;">
            <div class="col-sm-12" style="text-align: center;">
                <a class="btn btn-info" href="<%=request.getContextPath()%>/rotation/index">轮动监控</a>
            </div>
        </div>
    </div>
</body>
</html>
