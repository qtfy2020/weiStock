<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.weifly.weistock.web.WeistockBuilder" %>
<%
    String configCdn = (String)request.getAttribute(WeistockBuilder.ATTR_CONFIG_CDN);
%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1, user-scalable=no">
<%
    if("true".equals(configCdn)){
%>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
<script src="https://cdn.bootcss.com/jquery.form/3.51/jquery.form.js"></script>
<link href="https://cdn.bootcss.com/twitter-bootstrap/3.3.4/css/bootstrap.css" rel="stylesheet"/>
<script src="https://cdn.bootcss.com/twitter-bootstrap/3.3.4/js/bootstrap.js"></script>
<%--<script src="https://cdn.bootcss.com/echarts/4.3.0/echarts.min.js"></script>--%>
<%
    }else{
%>
<script src="<%=request.getContextPath()%>/static/js/jquery-3.3.1.js"></script>
<script src="<%=request.getContextPath()%>/static/js/jquery.form.js"></script>
<link href="<%=request.getContextPath()%>/static/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<script src="<%=request.getContextPath()%>/static/bootstrap/js/bootstrap.js"></script>
<%--<script src="<%=request.getContextPath()%>/static/js/echarts/echarts.js"></script>--%>
<%
    }
%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/static/css/stock.css" />