<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String rotationConfig = (String)request.getAttribute("rotationConfig");
    if(rotationConfig==null){
        rotationConfig = "{}";
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>轮动配置</title>
    <%@include file="/WEB-INF/jsp/layout/head.jsp"%>
    <style>
        .option-table td{
            white-space: nowrap;
            padding: 2px 2px 2px 2px;
        }
        .option-table .text-right{
            text-align: right;
        }
        .option-table .text-left{
            text-align: left;
        }
        .option-table .tit{
            font-weight: 600;
            text-align: center;
        }
        .total-table td{
            text-align: center;
        }
        .total-table td input{
            width: 100%;
        }
        .total-table td select{
            width: 100%;
        }
    </style>
    <script>
        var CONTEXT_PATH = "<%=request.getContextPath()%>";
        var rotationConfig = <%=rotationConfig%>;
        var updateConfigBtnObj;

        $(function () {
            renderInitConfig();
            initUpdateConfigButton();
        });

        function renderInitConfig() {
            if(rotationConfig.open!==undefined){
                if(rotationConfig.open){
                    $("#open").attr("checked", "checked");
                }else {
                    $("#open").removeAttr("checked");
                }
            }
            if(rotationConfig.baseStockCode!==undefined){
                $("#baseStockCode").val(rotationConfig.baseStockCode);
            }
            if(rotationConfig.baseStockName!==undefined){
                $("#baseStockName").val(rotationConfig.baseStockName);
            }
            if(rotationConfig.baseStockPrice!==undefined && rotationConfig.baseStockPrice>0){
                $("#baseStockPrice").val(rotationConfig.baseStockPrice);
            }
            if(rotationConfig.compareStockCode!==undefined){
                $("#compareStockCode").val(rotationConfig.compareStockCode);
            }
            if(rotationConfig.compareStockName!==undefined){
                $("#compareStockName").val(rotationConfig.compareStockName);
            }
            if(rotationConfig.compareStockPrice!==undefined && rotationConfig.compareStockPrice>0){
                $("#compareStockPrice").val(rotationConfig.compareStockPrice);
            }
        }

        function initUpdateConfigButton(){
            updateConfigBtnObj = $("#updateConfigBtn");
            updateConfigBtnObj.on("click", function(){
                $("#updateConfigForm").ajaxSubmit({
                    type: "post",
                    dataType: "json",
                    url: CONTEXT_PATH + "/rotation/updateRotationConfig",
                    beforeSubmit: function(arr, $form, options){
                        updateConfigBtnObj.attr("disabled", "disabled");
                        updateConfigBtnObj.html("保存中...");
                    },
                    success: function(result){
                        updateConfigBtnObj.removeAttr("disabled");
                        updateConfigBtnObj.html("保存");
                        if(result.code=="200"){
                            alert("保存成功");
                        }else{
                            alert(result.message);
                        }
                    }
                });
            });
        }
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12" style="padding:0px;">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/">首页</a></li>
                    <li class="breadcrumb-item"><a href="<%=request.getContextPath()%>/rotation/index">轮动首页</a></li>
                    <li class="breadcrumb-item active">轮动配置</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="overflow-x: auto;">
                <form id="updateConfigForm" method="post">
                    <table class="table table-bordered option-table total-table">
                        <tbody>
                            <tr>
                                <td class="tit">开启监控</td>
                                <td colspan="2" class="text-left"><input type="checkbox" id="open" name="open" value="true" checked style="width: auto;"></td>
                            </tr>
                            <tr>
                                <td class="tit">基准标的代码</td>
                                <td colspan="2" class="tit"><input type="text" id="baseStockCode" name="baseStockCode"></td>
                            </tr>
                            <tr>
                                <td class="tit">基准标的名称</td>
                                <td colspan="2" class="tit"><input type="text" id="baseStockName" name="baseStockName"></td>
                            </tr>
                            <tr>
                                <td class="tit">基准标的价格</td>
                                <td colspan="2" class="tit"><input type="text" id="baseStockPrice" name="baseStockPrice"></td>
                            </tr>
                            <tr>
                                <td class="tit">比较标的代码</td>
                                <td colspan="2" class="tit"><input type="text" id="compareStockCode" name="compareStockCode"></td>
                            </tr>
                            <tr>
                                <td class="tit">比较标的名称</td>
                                <td colspan="2" class="tit"><input type="text" id="compareStockName" name="compareStockName"></td>
                            </tr>
                            <tr>
                                <td class="tit">比较标的价格</td>
                                <td colspan="2" class="tit"><input type="text" id="compareStockPrice" name="compareStockPrice"></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button id="updateConfigBtn" class="btn btn-info">保存</button>
            </div>
        </div>

    </div>
</body>
</html>
