package com.weifly.weistock.rotation.bo;

/**
 * 轮动监控信息
 *
 * @author weifly
 * @since 2020/12/02
 */
public class RotationStatusBO {

    private String nowDiffTime; // 最新差值时间
    private double nowDiff; // 最新差值
    private String minDiffTime; // 最小差值时间
    private double minDiff; // 最小差值
    private String maxDiffTime; // 最大差值时间
    private double maxDiff; // 最大差值

    public String getNowDiffTime() {
        return nowDiffTime;
    }

    public void setNowDiffTime(String nowDiffTime) {
        this.nowDiffTime = nowDiffTime;
    }

    public double getNowDiff() {
        return nowDiff;
    }

    public void setNowDiff(double nowDiff) {
        this.nowDiff = nowDiff;
    }

    public String getMinDiffTime() {
        return minDiffTime;
    }

    public void setMinDiffTime(String minDiffTime) {
        this.minDiffTime = minDiffTime;
    }

    public double getMinDiff() {
        return minDiff;
    }

    public void setMinDiff(double minDiff) {
        this.minDiff = minDiff;
    }

    public String getMaxDiffTime() {
        return maxDiffTime;
    }

    public void setMaxDiffTime(String maxDiffTime) {
        this.maxDiffTime = maxDiffTime;
    }

    public double getMaxDiff() {
        return maxDiff;
    }

    public void setMaxDiff(double maxDiff) {
        this.maxDiff = maxDiff;
    }
}
