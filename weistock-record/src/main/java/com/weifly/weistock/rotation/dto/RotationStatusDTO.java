package com.weifly.weistock.rotation.dto;

import com.weifly.weistock.core.monitor.bo.MessageBO;
import com.weifly.weistock.rotation.bo.RotationPointBO;

import java.util.List;

/**
 * 轮动监控信息
 *
 * @author weifly
 * @since 2020/12/02
 */
public class RotationStatusDTO {

    private int status; // 状态
    private String date; // 日期
    private String time; // 时间
    private String baseStockCode; // 基准标的代码
    private String baseStockName; // 基准标的名称
    private String compareStockCode; // 比较标的代码
    private String compareStockName; // 比较标的名称
    private String nowDiffTime; // 最新差值时间
    private double nowDiff; // 最新差值
    private String minDiffTime; // 最小差值时间
    private double minDiff; // 最小差值
    private String maxDiffTime; // 最大差值时间
    private double maxDiff; // 最大差值
    private List<RotationPointBO> pointList; // 监控点列表
    private List<MessageBO> messageList; // 消息列表

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getBaseStockCode() {
        return baseStockCode;
    }

    public void setBaseStockCode(String baseStockCode) {
        this.baseStockCode = baseStockCode;
    }

    public String getBaseStockName() {
        return baseStockName;
    }

    public void setBaseStockName(String baseStockName) {
        this.baseStockName = baseStockName;
    }

    public String getCompareStockCode() {
        return compareStockCode;
    }

    public void setCompareStockCode(String compareStockCode) {
        this.compareStockCode = compareStockCode;
    }

    public String getCompareStockName() {
        return compareStockName;
    }

    public void setCompareStockName(String compareStockName) {
        this.compareStockName = compareStockName;
    }

    public String getNowDiffTime() {
        return nowDiffTime;
    }

    public void setNowDiffTime(String nowDiffTime) {
        this.nowDiffTime = nowDiffTime;
    }

    public double getNowDiff() {
        return nowDiff;
    }

    public void setNowDiff(double nowDiff) {
        this.nowDiff = nowDiff;
    }

    public String getMinDiffTime() {
        return minDiffTime;
    }

    public void setMinDiffTime(String minDiffTime) {
        this.minDiffTime = minDiffTime;
    }

    public double getMinDiff() {
        return minDiff;
    }

    public void setMinDiff(double minDiff) {
        this.minDiff = minDiff;
    }

    public String getMaxDiffTime() {
        return maxDiffTime;
    }

    public void setMaxDiffTime(String maxDiffTime) {
        this.maxDiffTime = maxDiffTime;
    }

    public double getMaxDiff() {
        return maxDiff;
    }

    public void setMaxDiff(double maxDiff) {
        this.maxDiff = maxDiff;
    }

    public List<RotationPointBO> getPointList() {
        return pointList;
    }

    public void setPointList(List<RotationPointBO> pointList) {
        this.pointList = pointList;
    }

    public List<MessageBO> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<MessageBO> messageList) {
        this.messageList = messageList;
    }
}
