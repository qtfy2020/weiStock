package com.weifly.weistock.rotation;

import com.weifly.weistock.core.market.StockMarketService;
import com.weifly.weistock.core.task.UpdateStatus;
import com.weifly.weistock.core.task.UpdateTask;

/**
 * 轮动更新任务
 *
 * @author weifly
 * @since 2020/12/02
 */
public class RotationUpdateTask implements UpdateTask {

    private StockMarketService stockMarketService;

    private RotationMonitorService rotationMonitorService;

    public void setStockMarketService(StockMarketService stockMarketService) {
        this.stockMarketService = stockMarketService;
    }

    public void setRotationMonitorService(RotationMonitorService rotationMonitorService) {
        this.rotationMonitorService = rotationMonitorService;
    }

    @Override
    public void doRunTask(UpdateStatus updateStatus) throws Exception {
        RotationCheckAction action = new RotationCheckAction();
        action.setStockMarketService(this.stockMarketService);
        action.setRotationMonitorService(this.rotationMonitorService);
        action.checkIfNeed(updateStatus);
    }
}
