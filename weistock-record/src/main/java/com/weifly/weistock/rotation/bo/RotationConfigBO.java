package com.weifly.weistock.rotation.bo;

/**
 * 轮动配置信息
 *
 * @author weifly
 * @since 2019/9/17
 */
public class RotationConfigBO {

    private boolean open; // 是否开启监控
    private String baseStockCode; // 基准标的代码
    private String baseStockName; // 基准标的名称
    private double baseStockPrice; // 基准标的价格
    private String compareStockCode; // 比较标的代码
    private String compareStockName; // 比较标的名称
    private double compareStockPrice; // 比较标的价格

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getBaseStockCode() {
        return baseStockCode;
    }

    public void setBaseStockCode(String baseStockCode) {
        this.baseStockCode = baseStockCode;
    }

    public String getBaseStockName() {
        return baseStockName;
    }

    public void setBaseStockName(String baseStockName) {
        this.baseStockName = baseStockName;
    }

    public double getBaseStockPrice() {
        return baseStockPrice;
    }

    public void setBaseStockPrice(double baseStockPrice) {
        this.baseStockPrice = baseStockPrice;
    }

    public String getCompareStockCode() {
        return compareStockCode;
    }

    public void setCompareStockCode(String compareStockCode) {
        this.compareStockCode = compareStockCode;
    }

    public String getCompareStockName() {
        return compareStockName;
    }

    public void setCompareStockName(String compareStockName) {
        this.compareStockName = compareStockName;
    }

    public double getCompareStockPrice() {
        return compareStockPrice;
    }

    public void setCompareStockPrice(double compareStockPrice) {
        this.compareStockPrice = compareStockPrice;
    }
}
