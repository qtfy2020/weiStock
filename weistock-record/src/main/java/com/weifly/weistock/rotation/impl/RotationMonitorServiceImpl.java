package com.weifly.weistock.rotation.impl;

import com.weifly.weistock.rotation.bo.RotationConfigBO;
import com.weifly.weistock.rotation.bo.RotationPointBO;
import com.weifly.weistock.rotation.RotationConverter;
import com.weifly.weistock.rotation.bo.RotationStatusBO;
import com.weifly.weistock.core.monitor.AbstractMonitorService;
import com.weifly.weistock.rotation.RotationConfigService;
import com.weifly.weistock.rotation.RotationMonitorService;
import com.weifly.weistock.rotation.dto.RotationStatusDTO;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * 轮动监控服务
 *
 * @author weifly
 * @since 2020/12/01
 */
public class RotationMonitorServiceImpl extends AbstractMonitorService implements RotationMonitorService {

    private RotationConfigBO rotationConfig; // 配置信息
    private RotationStatusBO rotationStatus = new RotationStatusBO(); // 状态信息
    private LinkedList<RotationPointBO> pointList = new LinkedList<>(); // 监控点集合

    private RotationConfigService rotationConfigService;

    public void setRotationConfigService(RotationConfigService rotationConfigService) {
        this.rotationConfigService = rotationConfigService;
    }

    @Override
    public void updateRotationConfig(RotationConfigBO configBO, boolean save) {
        this.rotationConfig = configBO;
        if (save) {
            this.rotationConfigService.saveRotationConfig(configBO);
        }
    }

    @Override
    public RotationConfigBO getRotationConfig() {
        return this.rotationConfig;
    }

    @Override
    public void clearMonitorInfo() {
        this.writeMessage("清空监控信息");
        RotationConverter.clear(this.rotationStatus);
        this.pointList.clear();
    }

    @Override
    public void addRotationPoint(RotationPointBO rotationPoint) {
        this.pointList.addFirst(rotationPoint);
        this.rotationStatus.setNowDiffTime(rotationPoint.getTime());
        this.rotationStatus.setNowDiff(rotationPoint.getDiff());
        if (this.rotationStatus.getMinDiffTime() == null || rotationPoint.getDiff() < this.rotationStatus.getMinDiff()) {
            this.rotationStatus.setMinDiffTime(rotationPoint.getTime());
            this.rotationStatus.setMinDiff(rotationPoint.getDiff());
        }
        if (this.rotationStatus.getMaxDiffTime() == null || rotationPoint.getDiff() > this.rotationStatus.getMaxDiff()) {
            this.rotationStatus.setMaxDiffTime(rotationPoint.getTime());
            this.rotationStatus.setMaxDiff(rotationPoint.getDiff());
        }
    }

    @Override
    public RotationStatusDTO calcMonitorInfo() {
        RotationStatusDTO statusDTO = new RotationStatusDTO();
        statusDTO.setStatus(this.status);
        statusDTO.setDate(this.date);
        statusDTO.setTime(this.time);
        RotationConverter.calcMonitorInfo(statusDTO, this.rotationConfig, this.rotationStatus);
        // 监控点列表
        statusDTO.setPointList(new ArrayList<>());
        for (RotationPointBO pointBO : this.pointList) {
            if (statusDTO.getPointList().size() >= 20) {
                break;
            }
            statusDTO.getPointList().add(pointBO);
        }
        // 消息列表
        statusDTO.setMessageList(new ArrayList<>(this.collectMessage()));
        return statusDTO;
    }
}
