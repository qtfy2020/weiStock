package com.weifly.weistock.rotation.bo;

/**
 * 监控点数据
 *
 * @author weifly
 * @since 2020/12/02
 */
public class RotationPointBO {

    private String time; // 监控时间
    private double basePrice; // 基准价格
    private double baseNormal; // 基准价格归一
    private double comparePrice; // 比较价格
    private double compareNormal; // 比较价格归一
    private double diff;// 归一化后的差值

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public double getBaseNormal() {
        return baseNormal;
    }

    public void setBaseNormal(double baseNormal) {
        this.baseNormal = baseNormal;
    }

    public double getComparePrice() {
        return comparePrice;
    }

    public void setComparePrice(double comparePrice) {
        this.comparePrice = comparePrice;
    }

    public double getCompareNormal() {
        return compareNormal;
    }

    public void setCompareNormal(double compareNormal) {
        this.compareNormal = compareNormal;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }
}
