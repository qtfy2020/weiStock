package com.weifly.weistock.record.option;

import com.weifly.weistock.record.bo.MergeRecordResult;
import com.weifly.weistock.record.option.domain.*;

import java.util.List;

/**
 * 交易记录管理服务
 *
 * @author weifly
 * @since 2019/11/18
 */
public interface OptionMonitorService {

    /**
     * 更新所有交易记录
     */
    void updateYearList(List<OptionYearDto> yearList);

    /**
     * 合并交易记录
     */
    MergeRecordResult mergeRecordList(List<OptionRecordDto> recordList);

    /**
     * 计算合约
     */
    void calcContract(CalcContractRequest request);

    /**
     * 加载记录列表
     */
    List<OptionRecordDto> getRecordList(GetRecordRequest recordRequest);

    /**
     * 加载期权统计
     */
    OptionSummaryDto loadSummaryInfo();
}
