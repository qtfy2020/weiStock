package com.weifly.weistock.record.option.domain;

/**
 * 期权统计信息
 *
 * @author weifly
 * @since 2019/11/30
 */
public class OptionSummaryDto {

    private double feeService; // 手续费
    private double diffAmount; // 配对结算金额
    private double diffAmountUp; // 配对结算金额 正值
    private double diffAmountDown; // 配对结算金额 负值

    public double getFeeService() {
        return feeService;
    }

    public void setFeeService(double feeService) {
        this.feeService = feeService;
    }

    public double getDiffAmount() {
        return diffAmount;
    }

    public void setDiffAmount(double diffAmount) {
        this.diffAmount = diffAmount;
    }

    public double getDiffAmountUp() {
        return diffAmountUp;
    }

    public void setDiffAmountUp(double diffAmountUp) {
        this.diffAmountUp = diffAmountUp;
    }

    public double getDiffAmountDown() {
        return diffAmountDown;
    }

    public void setDiffAmountDown(double diffAmountDown) {
        this.diffAmountDown = diffAmountDown;
    }
}
