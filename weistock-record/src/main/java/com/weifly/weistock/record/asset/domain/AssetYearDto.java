package com.weifly.weistock.record.asset.domain;

import com.weifly.weistock.core.util.visitor.DataListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * 某年的资产信息
 *
 * @author weifly
 * @since 2019/9/24
 */
public class AssetYearDto implements DataListContainer<AssetDayDto> {

    private String year; // 年，4位数字 2019
    private List<AssetDayDto> dayList = new ArrayList<>(); // 资金记录列表

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<AssetDayDto> getDayList() {
        return dayList;
    }

    public void setDayList(List<AssetDayDto> dayList) {
        this.dayList = dayList;
    }

    @Override
    public List<AssetDayDto> getDataList() {
        return dayList;
    }
}
