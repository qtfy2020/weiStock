package com.weifly.weistock.record.option.domain;

import com.weifly.weistock.record.option.OptionConst;

/**
 * 获取记录请求
 *
 * @author weifly
 * @since 2019/11/23
 */
public class GetRecordRequest {

    private String date; // 发生日期
    private String time; // 成交时间
    private String entrust; // 委托编号
    private OptionConst.OperationType operationType; // 买卖标志
    private String contractCode; // 合约编码

    // 过滤条件
    private String filterContractCode; // 合约编码
    private String filterPair; // 配对情况
    private int limit; // 返回记录数

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEntrust() {
        return entrust;
    }

    public void setEntrust(String entrust) {
        this.entrust = entrust;
    }

    public OptionConst.OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OptionConst.OperationType operationType) {
        this.operationType = operationType;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getFilterContractCode() {
        return filterContractCode;
    }

    public void setFilterContractCode(String filterContractCode) {
        this.filterContractCode = filterContractCode;
    }

    public String getFilterPair() {
        return filterPair;
    }

    public void setFilterPair(String filterPair) {
        this.filterPair = filterPair;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
