package com.weifly.weistock.record.option.domain;

import com.weifly.weistock.record.bo.AbstractRecordBO;
import com.weifly.weistock.record.option.OptionConst;

/**
 * 期权交易记录
 *
 * @author weifly
 * @since 2019/11/17
 */
public class OptionRecordDto extends AbstractRecordBO {
    // 共23个属性, 父类3个属性
    private String contractName; // 合约名称, 50ETF购9月2600
    private String contractCode; // 合约编码, 10001241
    private String stockCode; // 证券名称, 510050
    private String stockName; // 证券代码, 50ETF
    private OptionConst.BusinessName businessName; // 业务名称
    private OptionConst.OperationType operationType; // 买卖标志
    private OptionConst.HoldPosition holdPosition; // 持仓方向
    private Double tradePrice; // 成交价格;
    private Integer tradeNumber; // 成交数量
    private Double tradeAmount; // 成交金额
    private Double feeService; // 手续费
    private Double feeStamp; // 印花税
    private Double feeTransfer; // 过户费
    private Double feeExtra; // 附加费
    private Double feeClear; // 交易所清算费
    private Double clearAmount; // 清算金额
    private Double afterAmount; // 资金本次余额
    private Integer afterNumber; // 剩余数量

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public OptionConst.BusinessName getBusinessName() {
        return businessName;
    }

    public void setBusinessName(OptionConst.BusinessName businessName) {
        this.businessName = businessName;
    }

    public OptionConst.OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OptionConst.OperationType operationType) {
        this.operationType = operationType;
    }

    public OptionConst.HoldPosition getHoldPosition() {
        return holdPosition;
    }

    public void setHoldPosition(OptionConst.HoldPosition holdPosition) {
        this.holdPosition = holdPosition;
    }

    public Double getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(Double tradePrice) {
        this.tradePrice = tradePrice;
    }

    public Integer getTradeNumber() {
        return tradeNumber;
    }

    public void setTradeNumber(Integer tradeNumber) {
        this.tradeNumber = tradeNumber;
    }

    public Double getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Double tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Double getFeeService() {
        return feeService;
    }

    public void setFeeService(Double feeService) {
        this.feeService = feeService;
    }

    public Double getFeeStamp() {
        return feeStamp;
    }

    public void setFeeStamp(Double feeStamp) {
        this.feeStamp = feeStamp;
    }

    public Double getFeeTransfer() {
        return feeTransfer;
    }

    public void setFeeTransfer(Double feeTransfer) {
        this.feeTransfer = feeTransfer;
    }

    public Double getFeeExtra() {
        return feeExtra;
    }

    public void setFeeExtra(Double feeExtra) {
        this.feeExtra = feeExtra;
    }

    public Double getFeeClear() {
        return feeClear;
    }

    public void setFeeClear(Double feeClear) {
        this.feeClear = feeClear;
    }

    public Double getClearAmount() {
        return clearAmount;
    }

    public void setClearAmount(Double clearAmount) {
        this.clearAmount = clearAmount;
    }

    public Double getAfterAmount() {
        return afterAmount;
    }

    public void setAfterAmount(Double afterAmount) {
        this.afterAmount = afterAmount;
    }

    public Integer getAfterNumber() {
        return afterNumber;
    }

    public void setAfterNumber(Integer afterNumber) {
        this.afterNumber = afterNumber;
    }
}
