package com.weifly.weistock.optionvix.bo;

/**
 * 某天的VIX信息
 *
 * @author weifly
 * @since 2021/1/21
 */
public class VixDayBO {

    private String day; // 日期
    private Double openValue; // 开盘
    private Double highValue; // 最高
    private Double lowValue;  // 最低
    private Double closeValue; // 收盘

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Double getOpenValue() {
        return openValue;
    }

    public void setOpenValue(Double openValue) {
        this.openValue = openValue;
    }

    public Double getHighValue() {
        return highValue;
    }

    public void setHighValue(Double highValue) {
        this.highValue = highValue;
    }

    public Double getLowValue() {
        return lowValue;
    }

    public void setLowValue(Double lowValue) {
        this.lowValue = lowValue;
    }

    public Double getCloseValue() {
        return closeValue;
    }

    public void setCloseValue(Double closeValue) {
        this.closeValue = closeValue;
    }

    public void copyPropForm(VixDayBO vixDay) {
        this.day = vixDay.day;
        this.openValue = vixDay.openValue;
        this.highValue = vixDay.highValue;
        this.lowValue = vixDay.lowValue;
        this.closeValue = vixDay.closeValue;
    }
}
