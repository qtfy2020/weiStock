package com.weifly.weistock.optionvix.dto;

import java.util.List;

/**
 * VIX监控信息
 *
 * @author weifly
 * @since 2021/1/21
 */
public class VixSummaryDTO {

    private String newestDay; // 最新天
    private double newestVix; // 最新VIX

    private int recentDayNumber; // 近期天数
    private String recentStartDay; // 近期开始
    private String recentEndDay; // 近期结束
    private double recentPercent; // 近期百分位

    private int allDayNumber; // all天数
    private String allStartDay; // all开始
    private String allEndDay; // all结束
    private double allPercent; // all百分位

    private List<VixRegionDTO> regionList; // VIX区间集合

    public String getNewestDay() {
        return newestDay;
    }

    public void setNewestDay(String newestDay) {
        this.newestDay = newestDay;
    }

    public double getNewestVix() {
        return newestVix;
    }

    public void setNewestVix(double newestVix) {
        this.newestVix = newestVix;
    }

    public int getRecentDayNumber() {
        return recentDayNumber;
    }

    public void setRecentDayNumber(int recentDayNumber) {
        this.recentDayNumber = recentDayNumber;
    }

    public String getRecentStartDay() {
        return recentStartDay;
    }

    public void setRecentStartDay(String recentStartDay) {
        this.recentStartDay = recentStartDay;
    }

    public String getRecentEndDay() {
        return recentEndDay;
    }

    public void setRecentEndDay(String recentEndDay) {
        this.recentEndDay = recentEndDay;
    }

    public double getRecentPercent() {
        return recentPercent;
    }

    public void setRecentPercent(double recentPercent) {
        this.recentPercent = recentPercent;
    }

    public int getAllDayNumber() {
        return allDayNumber;
    }

    public void setAllDayNumber(int allDayNumber) {
        this.allDayNumber = allDayNumber;
    }

    public String getAllStartDay() {
        return allStartDay;
    }

    public void setAllStartDay(String allStartDay) {
        this.allStartDay = allStartDay;
    }

    public String getAllEndDay() {
        return allEndDay;
    }

    public void setAllEndDay(String allEndDay) {
        this.allEndDay = allEndDay;
    }

    public double getAllPercent() {
        return allPercent;
    }

    public void setAllPercent(double allPercent) {
        this.allPercent = allPercent;
    }

    public List<VixRegionDTO> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<VixRegionDTO> regionList) {
        this.regionList = regionList;
    }
}
