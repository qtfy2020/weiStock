package com.weifly.weistock.optionvix;

import com.weifly.weistock.optionvix.bo.VixDayExternalBO;
import com.weifly.weistock.optionvix.bo.VixYearBO;

import java.util.List;
import java.util.Map;

/**
 * VIX配置服务
 *
 * @author weifly
 * @since 2021/1/21
 */
public interface OptionVixConfigService {

    /**
     * 设置配置文件路径
     */
    void setConfigPath(String configPath);

    /**
     * 加载年份列表
     */
    List<VixYearBO> loadYearList();

    /**
     * 保存年VIX记录
     */
    void saveYear(VixYearBO yearBO);

    /**
     * 保存VIX附加信息
     */
    void saveDayExternal(Map<String, VixDayExternalBO> dayExternalMap);
}
