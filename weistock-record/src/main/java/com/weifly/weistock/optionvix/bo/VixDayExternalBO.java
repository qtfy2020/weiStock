package com.weifly.weistock.optionvix.bo;

/**
 * 某天的VIX附加信息
 *
 * @author weifly
 * @since 2021/1/27
 */
public class VixDayExternalBO {

    private String day; // 日期
    private double diff; // 差额
    private double rate; // 涨幅
    private double recentPercent; // recent百分位
    private double allPercent; // all百分位

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getRecentPercent() {
        return recentPercent;
    }

    public void setRecentPercent(double recentPercent) {
        this.recentPercent = recentPercent;
    }

    public double getAllPercent() {
        return allPercent;
    }

    public void setAllPercent(double allPercent) {
        this.allPercent = allPercent;
    }
}
