package com.weifly.weistock.optionvix;

/**
 * 常量定义
 *
 * @author weifly
 * Create at 2021/1/27
 */
public class OptionVixConstant {

    public static final int MAX_RECENT_DAY_NUMBER = 200; // recent最大查询天数
}
