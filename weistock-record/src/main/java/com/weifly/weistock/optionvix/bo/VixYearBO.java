package com.weifly.weistock.optionvix.bo;

import com.weifly.weistock.core.util.visitor.DataListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * 某年的VIX信息
 *
 * @author weifly
 * @since 2021/1/21
 */
public class VixYearBO implements DataListContainer<VixDayBO> {

    private String year; // 年，4位数字 2019
    private List<VixDayBO> dayList = new ArrayList<>(); // VIX记录列表

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<VixDayBO> getDayList() {
        return dayList;
    }

    public void setDayList(List<VixDayBO> dayList) {
        this.dayList = dayList;
    }

    @Override
    public List<VixDayBO> getDataList() {
        return dayList;
    }
}
