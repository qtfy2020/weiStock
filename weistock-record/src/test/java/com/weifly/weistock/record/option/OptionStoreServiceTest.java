package com.weifly.weistock.record.option;

import com.weifly.weistock.record.bo.MergeRecordResult;
import com.weifly.weistock.record.option.domain.OptionRecordDto;
import com.weifly.weistock.record.option.domain.OptionYearDto;
import com.weifly.weistock.record.option.impl.OptionMonitorServiceImpl;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 测试
 *
 * @author weifly
 * @since 2019/11/20
 */
public class OptionStoreServiceTest {

    @Test
    public void testSaveOptionYear() throws IOException {
        OptionStoreService optionStoreService = OptionTestUtils.createOptionStoreService();
        OptionMonitorServiceImpl optionMonitorService = new OptionMonitorServiceImpl();
        optionMonitorService.setOptionStoreService(optionStoreService);
        OptionParseService optionParseService = OptionTestUtils.createOptionParseService();

        // 从xml加载数据
        List<OptionYearDto> yearList = optionStoreService.loadYearList();
        optionMonitorService.updateYearList(yearList);

        // 解析交割单并保存
        File recordFile = new File("d:/weistock/test.txt");
        List<OptionRecordDto> recordList = optionParseService.parseRecord(recordFile);
        MergeRecordResult result = optionMonitorService.mergeRecordList(recordList);
        System.out.println(result.desc());
    }
}
