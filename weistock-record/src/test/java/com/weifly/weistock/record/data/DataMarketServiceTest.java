package com.weifly.weistock.record.data;

import com.weifly.weistock.core.market.bo.StockDayBO;
import com.weifly.weistock.core.market.bo.StockKLineBO;
import com.weifly.weistock.core.market.impl.SinaStockKLineV2Service;
import org.junit.Test;

/**
 * 测试
 *
 * @author weifly
 * @since 2019/9/29
 */
public class DataMarketServiceTest {

    @Test
    public void testLoadStockData() {
        SinaStockKLineV2Service dataMarketService = new SinaStockKLineV2Service();
        StockKLineBO stockKLineBO = dataMarketService.loadStockKLine("sh510050");
        if (stockKLineBO != null) {
            for (StockDayBO dayBO : stockKLineBO.getDayList()) {
                System.out.println(dayBO.getDay() + " : " + dayBO.getOpen() + " : " + dayBO.getHigh() + " : " + dayBO.getLow() + " : " + dayBO.getClose());
            }
        }
    }
}
