package com.weifly.weistock.record.asset;

import com.weifly.weistock.record.asset.domain.AssetDayDto;
import com.weifly.weistock.record.asset.impl.AssetMonitorServiceImpl;
import org.junit.Test;

/**
 * 测试
 *
 * @author weifly
 * @since 2019/9/24
 */
public class AssetMonitorServiceTest {

    @Test
    public void testUpdateAsset(){
        AssetConfigService assetConfigService = AssetTestUtils.createAssetConfigService();
        AssetMonitorServiceImpl assetMonitorService = new AssetMonitorServiceImpl();
        assetMonitorService.setAssetConfigService(assetConfigService);

        AssetDayDto dayDto = AssetTestUtils.createAssetDay();
        assetMonitorService.updateAsset(dayDto);
    }
}
