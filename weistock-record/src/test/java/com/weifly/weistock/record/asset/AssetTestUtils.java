package com.weifly.weistock.record.asset;

import com.weifly.weistock.record.asset.domain.AssetDayDto;
import com.weifly.weistock.record.asset.impl.AssetConfigServiceImpl;

/**
 * 单元测试相关方法
 *
 * @author weifly
 * @since 2019/9/24
 */
public class AssetTestUtils {

    public static AssetConfigService createAssetConfigService() {
        AssetConfigServiceImpl assetConfigService = new AssetConfigServiceImpl();
        assetConfigService.setConfigPath("d:/weistock/asset");
        return assetConfigService;
    }

    public static AssetDayDto createAssetDay(){
        AssetDayDto dayDto = new AssetDayDto();
        dayDto.setDay("20190901");
        dayDto.setAsset(100500);
        return dayDto;
    }
}
