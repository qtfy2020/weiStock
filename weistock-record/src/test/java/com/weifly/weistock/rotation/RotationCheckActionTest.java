package com.weifly.weistock.rotation;

import com.weifly.weistock.rotation.RotationCheckAction;
import com.weifly.weistock.rotation.bo.RotationConfigBO;
import com.weifly.weistock.core.market.impl.SinaStockMarketService;
import com.weifly.weistock.rotation.impl.RotationMonitorServiceImpl;
import org.junit.Test;

public class RotationCheckActionTest {

    @Test
    public void testCheck(){
        RotationMonitorServiceImpl monitorService = new RotationMonitorServiceImpl();
        monitorService.updateRotationConfig(this.buildRotationConfig(), false);

        RotationCheckAction checkAction = new RotationCheckAction();
        checkAction.setStockMarketService(new SinaStockMarketService());
        checkAction.setRotationMonitorService(monitorService);
        checkAction.check();
    }

    private RotationConfigBO buildRotationConfig(){
        RotationConfigBO configBO = new RotationConfigBO();
        configBO.setOpen(true);
        configBO.setBaseStockCode("510300");
        configBO.setBaseStockName("300ETF");
        configBO.setBaseStockPrice(4.272);
        configBO.setCompareStockCode("163417");
        configBO.setCompareStockName("兴全合宜");
        configBO.setCompareStockPrice(1.4301);
        return configBO;
    }
}
