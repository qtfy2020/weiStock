功能记录：
更新股票账户
添加网格策略
更新网格策略
实时监控页面

20190808
问题：java调用trade.dll时报“java.lang.UnsatisfiedLinkError: 找不到指定的模块”
原因：需要安装微软的依赖库，参考：https://blog.csdn.net/u010727189/article/details/76688189
下载地址：https://www.microsoft.com/zh-cn/download/details.aspx?id=40784
安装文件：vcredist_x86.exe

20191111
问题：java通过dll连接交易服务器报错时，算严重错误，后续不再连接服务器。频繁连接服务器，会受到券商的警告。